/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
/**Class to Create StandardObject in server
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 *
 * 
 */
public class StandardObject implements Serializable
{
   private String protocol;
    private Object object;
/**Method to initialize object
 * @param object 
 * @param protocol 
 * @author user
 */
    public StandardObject(String protocol, Object object)
    {
        this.protocol = protocol;
        this.object = object;
    }
/**Method to get the protocol
 * 
 * @return protocol
 */
    public String getProtocol()
    {
        return protocol;
    }
/**Mthod to set de protocol
 *
 * @param protocol 
 */
    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }
/**Method to get the object
 * 
 * @return object
 */
    public Object getObject()
    {
        return object;
    }
/**Method to set de object
 * 
 * @param object 
 */
    public void setObject(Object object)
    {
        this.object = object;
    }
}

