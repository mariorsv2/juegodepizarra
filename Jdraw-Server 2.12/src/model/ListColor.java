/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import model.Colors;
import java.awt.Color;

/**
 *
 * @author Ryu Alejo
 */
public class ListColor implements Serializable
{
    //Color color;
    private List <Color> listColor;
    
    public ListColor()
    {
        this.listColor = new ArrayList();
    }
    
    public Iterator iterator()
    {
        return this.listColor.iterator();
    }
    
    public boolean addColor(Color color)
    {
        return this.listColor.add(color);
    }
    
    public boolean removeColor(Color color)
    {
        return this.listColor.remove(color);
    }
    
    
    
    public void printAllColors()
    {
        Iterator    iterator;
        Color     actualColor;
        
        iterator = this.listColor.iterator();
        
        while (iterator.hasNext())
        {
            System.out.println("hola");
            actualColor = (Color) iterator.next();
            String colores=actualColor.toString();
            Color colorName = Color.decode(colores);
            System.out.println(colorName);
        }
    }
    
    public Color getColors()
    {
        
        Iterator    iterator;
        Color     actualColor;
        actualColor = null;
        iterator = this.listColor.iterator();
        
        while (iterator.hasNext())
        {
            System.out.println("entre");
            actualColor = (Color) iterator.next();
            return actualColor;
        }
        return actualColor;
        
    }
    
}

