/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Ryu Alejo
 */
public class StoreXmlFile 
{
    public static boolean saveUserDataStoreInDataBase (String nickname, String bombs, String coins)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilStoreXml.STORE_XML_PATH);

            root = doc.getRootElement();


            newChild = new Element(UtilStoreXml.STORE_TAG);

  
            newChild.setAttribute(UtilStoreXml.STORE_NICKNAME_TAG, nickname);
            newChild.setAttribute(UtilStoreXml.STORE_BOMBS_TAG, bombs);
            newChild.setAttribute(UtilStoreXml.STORE_COINS_TAG, coins);

            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilStoreXml.STORE_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    } 
    
    public static boolean readNicknameinStoreJDrawFromDataBase(String nicknameuser)
    {
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilStoreXml.STORE_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilStoreXml.STORE_NICKNAME_TAG);
                

                if( nickname!= null      && nickname.equals(nicknameuser))
                {
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println("Ya hay Un Usuario Con el Nickname");

                   

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
    
    public static String readBombsFromStoreDataBase(String userNickname)
    {
        
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,oldBombs;
        String          bombs="";
        boolean found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilStoreXml.STORE_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilStoreXml.STORE_NICKNAME_TAG);
                oldBombs         = child.getAttributeValue(UtilStoreXml.STORE_BOMBS_TAG);

                if( nickname!= null      && nickname.equals(userNickname))
                {
                    bombs=oldBombs;
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println("Ya hay Un Usuario Con el Nickname");

                   

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        
        return bombs;
        
    }
    public static String readCoinsFromStoreDataBase(String userNickname)
    {
        
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,oldcoins,coins="";
        boolean found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilStoreXml.STORE_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilStoreXml.STORE_NICKNAME_TAG);
                oldcoins         = child.getAttributeValue(UtilStoreXml.STORE_COINS_TAG);

                if( nickname!= null      && nickname.equals(userNickname))
                {
                    coins=oldcoins;
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println("Ya hay Un Usuario Con el Nickname");

                   

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        
        return coins;
        
    }
    public static boolean saveNewBombsInDataBase (String nickname, String bombs , String Coins)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilStoreXml.STORE_XML_PATH);

            root = doc.getRootElement();
            
            

            newChild = new Element(UtilStoreXml.STORE_TAG);

            //String bom=
            newChild.setAttribute(UtilStoreXml.STORE_NICKNAME_TAG, nickname);
            newChild.setAttribute(UtilStoreXml.STORE_BOMBS_TAG, bombs);
            newChild.setAttribute(UtilStoreXml.STORE_COINS_TAG, Coins);
            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilStoreXml.STORE_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
    
    public static boolean updateCoinsInDataBase (String Coins)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilStoreXml.STORE_XML_PATH);

            root = doc.getRootElement();
            
            

            newChild = new Element(UtilStoreXml.STORE_TAG);

            
            newChild.setAttribute(UtilStoreXml.STORE_COINS_TAG, Coins);
            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilStoreXml.STORE_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
    
    public static boolean removeUserFromStoreDataBase(String nicknameuser)
    {
        Document        doc;
        Element         root,child = null;
        List <Element>  rootChildrens;
        String          nickname;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilStoreXml.STORE_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilStoreXml.STORE_NICKNAME_TAG);
       
                if( nickname!= null      && nickname.equals(nicknameuser))
                {
                    System.out.println("entro3");
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(UtilStoreXml.ERROR_STORE_NICKNAME_TAG);
                    pos++;
                }
            }
            if (found){
                root.removeContent(child);
            }
            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilStoreXml.STORE_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            }
        
        catch(JDOMParseException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilStoreXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
}
