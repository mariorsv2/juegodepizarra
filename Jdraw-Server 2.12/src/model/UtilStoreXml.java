/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;



/**
 *
 * @author Ryu Alejo
 */
public class UtilStoreXml 
{
    public static final String STORE_TAG = "Store";
    public static final String STORE_NICKNAME_TAG = "Nickname";
    public static final String STORE_BOMBS_TAG = "Bombs";
    public static final String STORE_COINS_TAG ="Coins";
    public static final String STORE_COLORPALETTE_TAG = "ColorPalette";
    
    public static final String ERROR_STORE_NICKNAME_TAG = "Error loading Store Data from XML - Error in the attribute " + STORE_NICKNAME_TAG + " of the XML tag";
    public static final String ERROR_STORE_BOMBS_TAG = "Error loading Store Data from XML - Error in the attribute " + STORE_BOMBS_TAG + " of the XML tag";
    public static final String ERROR_STORE_COINS_TAG = "Error loading Store Data from XML - Error in the attribute " + STORE_COINS_TAG + " of the XML tag";
    public static final String ERROR_STORE_COLORPALETTE_TAG = "Error loading User from XML - Error in the attribute " + STORE_COLORPALETTE_TAG + " of the XML tag";
   
   

    public static final String ERROR_XML_EMPTY_FILE = "Error loading XML file - The file is empty";
    public static final String ERROR_XML_PROCESSING_FILE = "Error loading XML file - It's not possible processing the file";
    public static final String ERROR_XML_PROFESSOR_ID_NOT_EXIST = "Error loading XML file - The professor don't exist";
    
    
    public static final String STORE_XML_PATH = "src/model/store.xml";
}
