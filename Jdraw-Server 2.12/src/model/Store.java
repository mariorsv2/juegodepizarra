/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Ryu Alejo
 */
public class Store implements Serializable
{
    int bombs;
    int coins;

    public Store(int bombs, int coins) {
        this.bombs = bombs;
        this.coins = coins;
    }

    public Store(int bombs) {
        this.bombs = bombs;
    }


    public int getBombs() {
        return bombs;
    }

    public void setBombs(int bombs) {
        this.bombs = bombs;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
    
    
}
