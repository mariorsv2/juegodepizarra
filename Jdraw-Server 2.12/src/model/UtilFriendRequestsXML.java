/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**Class For initializes parameters for FriendRequestXmlFile
 *
 * @author user
 */
public class UtilFriendRequestsXML {
    public static final String FRIENDREQUEST_TAG = "InviteRequest";
    public static final String FRIENDNICKNAME_TAG = "Nickname";
    public static final String FRIENDNICKNAMEFRIENDREQUEST_TAG = "RequestFriendNicknameFriend";

    
    public static final String ERROR_FRIEND_NICKNAME_TAG = "Error loading User from XML - Error in the attribute " + FRIENDNICKNAME_TAG + " of the XML tag";
    public static final String ERROR_FRIEND_NICKNAMEFRIENDREQUEST_TAG = "Error loading User from XML - Error in the attribute " + FRIENDNICKNAMEFRIENDREQUEST_TAG + " of the XML tag";
    
   
   

    public static final String ERROR_XML_EMPTY_FILE = "Error loading XML file - The file is empty";
    public static final String ERROR_XML_PROCESSING_FILE = "Error loading XML file - It's not possible processing the file";
    public static final String ERROR_XML_PROFESSOR_ID_NOT_EXIST = "Error loading XML file - The professor don't exist";
    
    
    public static final String FRIENDSREQUEST_XML_PATH = "src/model/friendrequests.xml";
    
}
