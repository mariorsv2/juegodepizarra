/*
 * This class contents the methods to confirm the correct typing of password
 * when the user log in or register
 */
package model;

import java.util.Comparator;

/**Class to compare passwords of user
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */
public class PasswordComparator implements Comparator
{
/**Method that returns the largest passwords
 * @param firstObject 
 * @param secondObject 
 * @return int
 * @author user
 */
    @Override
 public int compare(Object firstObject, Object secondObject)
    {
        int result = ((User) firstObject).getPassword().compareTo(((User) secondObject).getPassword());
        int x = result;
        
        if (result <= 0)
            return -1;
        
        return 1;
        
       
        
    }
    
 }
