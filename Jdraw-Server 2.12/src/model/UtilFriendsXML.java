/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**Class For initializes parameters for FriendsXmlFile
 *
 * @author user
 */
public class UtilFriendsXML {
    public static final String FRIENDS_TAG = "FRIENDS";
    public static final String FRIENDNICKNAME_TAG = "Nickname";
    public static final String FRIENDNICKNAMEFRIEND_TAG = "NicknameFriend";

    
    public static final String ERROR_FRIEND_NICKNAME_TAG = "Error loading User from XML - Error in the attribute " + FRIENDNICKNAME_TAG + " of the XML tag";
    public static final String ERROR_FRIEND_NICKNAMEFRIEND_TAG = "Error loading User from XML - Error in the attribute " + FRIENDNICKNAMEFRIEND_TAG+ " of the XML tag";
    
   
   

    public static final String ERROR_XML_EMPTY_FILE = "Error loading XML file - The file is empty";
    public static final String ERROR_XML_PROCESSING_FILE = "Error loading XML file - It's not possible processing the file";
    public static final String ERROR_XML_PROFESSOR_ID_NOT_EXIST = "Error loading XML file - The professor don't exist";
    
    
    public static final String FRIENDS_XML_PATH = "src/model/friends.xml";
}
