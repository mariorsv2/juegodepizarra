/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**Class for Manage FriendXMLFile in the system
 *
 * @author Ruben Loaiza
 * @author Mario Salazar
 */
public class UserXmlFile
{
/**Method to LogIn Users
 * @param nicknameuser 
 * @param passworduser
 * @return boolean
 * @author user
 */
    public static boolean readLogInFromDataBase(String nicknameuser, String passworduser)
    {
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname, password;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(Util.USER_NICKNAME_TAG);
                password        = child.getAttributeValue(Util.USER_PASSWORD_TAG);
                

                if( nickname!= null      && nickname.equals(nicknameuser) &&
                   password  != null     && password.equals(passworduser))
                {
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(Util.ERROR_USER_NICKNAME_TAG);

                    if (password == null)
                        System.out.println(Util.ERROR_USER_PASSWORD_TAG);
                
                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
 /**Method to Read Nickname From XMLfile
 * @param nicknameuser 
 * @return boolean
 * @author user
 */
    public static boolean readNicknameinJDrawFromDataBase(String nicknameuser)
    {
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(Util.USER_NICKNAME_TAG);
                

                if( nickname!= null      && nickname.equals(nicknameuser))
                {
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println("Ya hay Un Usuario Con el Nickname");

                   

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
 /**Method to Read Avatars From XMLfile
 * @param nicknameuser
 * @param path 
 * @return boolean
 * @author user
 */
     public static void AvatarinJDrawFromDataBase(String nicknameuser,String path)
    {
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,pathuser;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(Util.USER_NICKNAME_TAG);
                pathuser         = child.getAttributeValue(Util.USER_AVATAR_TAG);

                if( nickname!= null      && nickname.equals(nicknameuser))
                {
                    path = pathuser;
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(Util.ERROR_USER_NICKNAME_TAG);

                   

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

    
    }
 /**Method to Read User From XMLfile
  * @param nicknameuser 
 * @return User
 * @author user
 */
    public static User readUserFromJDrawDataBase(String nicknameuser) throws ParseException 
    {
        User            user = null;
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          name, firstlastName, secondLastName, nickname, email, password, avatar, birthdate;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                name            = child.getAttributeValue(Util.USER_NAME_TAG);
                firstlastName   = child.getAttributeValue(Util.USER_FIRSTLASTNAME_TAG);
                secondLastName  = child.getAttributeValue(Util.USER_SECONDLASTNAME_TAG);
                nickname        = child.getAttributeValue(Util.USER_NICKNAME_TAG);
                email           = child.getAttributeValue(Util.USER_EMAIL_TAG);
                birthdate       = child.getAttributeValue(Util.USER_DATEOFBIRTH_TAG);
                password        = child.getAttributeValue(Util.USER_PASSWORD_TAG);
                avatar          = child.getAttributeValue(Util.USER_AVATAR_TAG);
                
                

                if(name != null && firstlastName != null && secondLastName != null &&
                   nickname.equals(nicknameuser))
                {
                    user = new User(name, firstlastName, secondLastName, UtilFunctions.convertToDate(birthdate), nickname,password, email, avatar);
                    found = true;
                }
                else
                {
                    if (name == null)
                        System.out.println(Util.ERROR_USER_NAME_TAG);

                    if (firstlastName == null)
                        System.out.println(Util.ERROR_USER_FIRSTLASTNAME_TAG);
                    
                    if (nickname == null)
                        System.out.println(Util.ERROR_USER_NICKNAME_TAG);
                    
                    if (secondLastName == null)
                        System.out.println(Util.ERROR_USER_SECONDLASTNAME_TAG);
                    
                  
                    
                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return user;
    }
 /**Method to All users From XMLfile
 * @param listuser 
 * @author user
 */
    public static void readAllUsersFromJDrawDataBase(ListUser listuser)
    {
        User            user = null;
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          name, firstlastName, secondLastName, nickname;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                name            = child.getAttributeValue(Util.USER_NAME_TAG);
                firstlastName        = child.getAttributeValue(Util.USER_FIRSTLASTNAME_TAG);
                secondLastName             = child.getAttributeValue(Util.USER_SECONDLASTNAME_TAG);
               
                nickname        = child.getAttributeValue(Util.USER_NICKNAME_TAG);

                if(name != null && firstlastName != null && secondLastName != null && nickname != null)
                {
                    user = new User(name, firstlastName, secondLastName, nickname);
                    
                    listuser.addUser(user);
                }
                else
                {
                    if (name == null)
                        System.out.println(Util.ERROR_USER_NAME_TAG);

                    if (firstlastName == null)
                        System.out.println(Util.ERROR_USER_FIRSTLASTNAME_TAG);

                    if (secondLastName == null)
                        System.out.println(Util.ERROR_USER_SECONDLASTNAME_TAG);
                    
              
                    
                    if (nickname == null )
                        System.out.println(Util.ERROR_USER_NICKNAME_TAG);
                }
                
                pos++;
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
    }
 /**Method to Save User From XMLfile
  * @param user
 * @return boolean
 * @author user
 */
    public static boolean saveUserInJDrawDataBase (User user)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();


            newChild = new Element(Util.USER_TAG);

  
            newChild.setAttribute(Util.USER_NAME_TAG, user.getName());
            newChild.setAttribute(Util.USER_FIRSTLASTNAME_TAG, user.getFirstLastName());
            newChild.setAttribute(Util.USER_SECONDLASTNAME_TAG, user.getSecondLastName());
            newChild.setAttribute(Util.USER_DATEOFBIRTH_TAG, UtilFunctions.convertToString(user.getBirthdate()));
            newChild.setAttribute(Util.USER_NICKNAME_TAG, user.getNickname());
            newChild.setAttribute(Util.USER_PASSWORD_TAG, user.getPassword());
            newChild.setAttribute(Util.USER_EMAIL_TAG,user.getEmail());
            newChild.setAttribute(Util.USER_AVATAR_TAG,user.getAvatar());

            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(Util.USERS_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
/**Method to Remove User in Usersxml
 * @param nicknameuser 
 * 
 * @return boolean
 * 
 */
    public static boolean removeUserFromJDrawDataBase(String nicknameuser)
    {
        Document        doc;
        Element         root,child = null;
        List <Element>  rootChildrens;
        String          nickname;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(Util.USER_NICKNAME_TAG);
       
                if( nickname!= null      && nickname.equals(nicknameuser))
                {
                    System.out.println("entro3");
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(Util.ERROR_USER_NICKNAME_TAG);
                    pos++;
                }
            }
            if (found){
                root.removeContent(child);
            }
            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(Util.USERS_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            }
        
        catch(JDOMParseException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
/**Method to Update User information in UserXmlFile
 * @param user 
 * 
 * @return boolean
 * 
 */
    
    public static boolean updateUserInJDrawDataBase (User user)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(Util.USERS_XML_PATH);

            root = doc.getRootElement();


            newChild = new Element(Util.USER_TAG);

  
            newChild.setAttribute(Util.USER_NAME_TAG, user.getName());
            newChild.setAttribute(Util.USER_FIRSTLASTNAME_TAG, user.getFirstLastName());
            newChild.setAttribute(Util.USER_SECONDLASTNAME_TAG, user.getSecondLastName());
            newChild.setAttribute(Util.USER_DATEOFBIRTH_TAG, UtilFunctions.convertToString(user.getBirthdate()));
            newChild.setAttribute(Util.USER_NICKNAME_TAG, user.getNickname());
            newChild.setAttribute(Util.USER_PASSWORD_TAG, user.getPassword());
            newChild.setAttribute(Util.USER_EMAIL_TAG,user.getEmail());
            newChild.setAttribute(Util.USER_AVATAR_TAG,user.getAvatar());

            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(Util.USERS_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
}
