/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Ryu Alejo
 */
public class PaletteXmlFile 
{
    
    public static boolean searchPurchasedPalettesFromStoreDataBase(String userNickname, String newPalette)
    {
        
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,oldPalettes;
        String          palette="";
        boolean found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilPaletteXml.PALETTE_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilPaletteXml.PALETTE_USERNICKNAME_TAG);
                oldPalettes      = child.getAttributeValue(UtilPaletteXml.PALETTE_PALETTENAME_TAG);

                if( nickname!= null      && nickname.equals(userNickname))
                {
                    palette=oldPalettes;
                    System.out.println("ENTRO");
                    String[] savedPalettes= palette.split(",");
                    int a =0;
                    while(a<=3)
                    {
                        System.out.println(savedPalettes[a]);
                        if(savedPalettes[a].compareTo(newPalette)==0)
                        {
                            found =true;
                        }
                        a++;
                      
                    }
                     
                }
                else
                {
                    if (nickname == null)
                        System.out.println("No saved palettes");
                    pos++;
                }
            }
        }
     catch(JDOMParseException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        
        return found;
        
    }
    public static String readPalettesFromStoreDataBase(String userNickname)
    {
        
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,oldPalettes;
        String          palette="";
        boolean found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilPaletteXml.PALETTE_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilPaletteXml.PALETTE_USERNICKNAME_TAG);
                oldPalettes         = child.getAttributeValue(UtilPaletteXml.PALETTE_PALETTENAME_TAG);

                if( nickname!= null      && nickname.equals(userNickname))
                {
                    palette=oldPalettes;
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println("No saved palettes");

                   

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        
        return palette;
        
    }
    
    
    public static boolean saveNewPaletteInDataBase (String nickname, String paletteName)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilPaletteXml.PALETTE_XML_PATH);

            root = doc.getRootElement();
            
            

            newChild = new Element(UtilPaletteXml.PALETTE_TAG);
            newChild.setAttribute(UtilPaletteXml.PALETTE_USERNICKNAME_TAG, nickname);
            newChild.setAttribute(UtilPaletteXml.PALETTE_PALETTENAME_TAG, paletteName);
   
            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilPaletteXml.PALETTE_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
     public static boolean removeUserFromPaletteDataBase(String nicknameuser)
    {
        Document        doc;
        Element         root,child = null;
        List <Element>  rootChildrens;
        String          nickname;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilPaletteXml.PALETTE_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilPaletteXml.PALETTE_USERNICKNAME_TAG);
       
                if( nickname!= null      && nickname.equals(nicknameuser))
                {
                    System.out.println("entro3");
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(UtilPaletteXml.ERROR_PALETTE_USERNICKNAME_TAG);
                    pos++;
                }
            }
            if (found){
                root.removeContent(child);
            }
            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilPaletteXml.PALETTE_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            }
        
        catch(JDOMParseException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilPaletteXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
}
