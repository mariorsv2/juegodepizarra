/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import model.FriendsXMLFile;

/**Class for Manage FriendRequestXMLFile in the system
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 *
 * 
 */
public class FriendRequestsXMLFile 
{
 
    
 /**Method to read All friends from xmlfile
 * @param listrequest
 * @param nicknameuser
 * 
 */
    public static void readAllFriendsFromJDrawDataBase(ListRequest listrequest,String nicknameuser) throws ParseException
    {
        User            user = null;
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,nicknamefriend;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilFriendRequestsXML.FRIENDSREQUEST_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);
              
                nickname            = child.getAttributeValue(UtilFriendRequestsXML.FRIENDNICKNAME_TAG);
                nicknamefriend      = child.getAttributeValue(UtilFriendRequestsXML.FRIENDNICKNAMEFRIENDREQUEST_TAG);
                

                if((nickname!=null && nickname.equals(nicknameuser)))
                {
                    user=UserXmlFile.readUserFromJDrawDataBase(nicknamefriend);
                    listrequest.addFriend(user);
                }
    
                else
                {
                    if (nicknamefriend == null)
                        System.out.println(UtilFriendRequestsXML.FRIENDNICKNAMEFRIENDREQUEST_TAG);
                    if (nickname == null )
                        System.out.println(UtilFriendRequestsXML.FRIENDNICKNAME_TAG);
                }
                
                pos++;
            }
        }
              catch(JDOMParseException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
    }
/**Method to Save Request in xmlfile
 * @param nickname
 * @param nicknamefriend
 * @return boolean
 * @author user
 */
    public static boolean saveRequestUserInJDrawDataBase (String nickname, String nicknamefriend)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilFriendRequestsXML.FRIENDSREQUEST_XML_PATH);

            root = doc.getRootElement();


            newChild = new Element(UtilFriendRequestsXML.FRIENDREQUEST_TAG);

  
            newChild.setAttribute(UtilFriendRequestsXML.FRIENDNICKNAMEFRIENDREQUEST_TAG, nickname);
            newChild.setAttribute(UtilFriendRequestsXML.FRIENDNICKNAME_TAG, nicknamefriend);


            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilFriendRequestsXML.FRIENDSREQUEST_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
             catch(JDOMParseException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
/**Method to Remove Request in xmlfile
 * @param nicknameuser
 * @param nicknameuserfriend 
 * @return boolean
 * @author user
 */
    public static boolean removeRequestUserFromJDrawDataBase(String nicknameuser, String nicknameuserfriend)
    {
        Document        doc;
        Element         root,child = null;
        List <Element>  rootChildrens;
        String          nickname, nicknamefriend;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilFriendRequestsXML.FRIENDSREQUEST_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilFriendRequestsXML.FRIENDNICKNAME_TAG);
                nicknamefriend        = child.getAttributeValue(UtilFriendRequestsXML.FRIENDNICKNAMEFRIENDREQUEST_TAG);
                

                if( nickname!= null      && nickname.equals(nicknameuser) &&
                   nicknamefriend  != null     && nicknamefriend.equals(nicknameuserfriend))
                {
                    System.out.println("entro3");
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(UtilFriendRequestsXML.ERROR_FRIEND_NICKNAME_TAG);

                    if (nicknameuserfriend == null)
                        System.out.println(UtilFriendRequestsXML.ERROR_FRIEND_NICKNAMEFRIENDREQUEST_TAG);
                
                    
                    pos++;
                }
            }
            if (found){
                root.removeContent(child);
            }
            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilFriendRequestsXML.FRIENDSREQUEST_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            }
        
        catch(JDOMParseException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendRequestsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
}
