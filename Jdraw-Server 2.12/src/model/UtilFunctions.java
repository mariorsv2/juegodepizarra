/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**Class For Convert Date to String and String to Date
 *
 * @author user
 */
public class UtilFunctions {
 /**Method For Convert Date to String
 *
 * @author user
 */
    public static String convertToString( Date date){
        
        DateFormat df;
        String   dateString = null;
              
          df = new SimpleDateFormat("dd/MMM/YYYY");
          dateString = df.format(date);
          
          return dateString;

    }
 /**Method For Convert String to Date
 *
 * @author user
 */
    public static Date convertToDate (String stringDate) throws ParseException{
        
        DateFormat df;
        Date      date=new Date();
         df = new SimpleDateFormat("dd/MMM/YYYY");
         
         date = df.parse(stringDate);
         
         return date;
    }
}
