/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Comparator;

/**Class to compare firstlastname of user
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 *
 * 
 */
public class FirstLastNameComparator implements Comparator,Serializable
{
 /**Method that returns the largest surname
 * @param firstObject 
 * @param secondObject
 * @return int 
 * 
 */
    @Override

    public int compare(Object firstObject, Object secondObject)
    {
        int result = ((User) firstObject).getFirstLastName().compareTo(((User) secondObject).getFirstLastName());
        
        if (result <= 0)
            return -1;
        
        return 1;
    }
}
    

