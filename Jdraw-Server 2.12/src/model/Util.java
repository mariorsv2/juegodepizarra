/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**Class For initializes parameters for UserXmlFile
 *
 * @author user
 */
public class Util {
    public static final String USER_TAG = "User";
    public static final String USER_NAME_TAG = "Name";
    public static final String USER_FIRSTLASTNAME_TAG = "FirstlastName";
    public static final String USER_SECONDLASTNAME_TAG ="Secondlastname";
    public static final String USER_NICKNAME_TAG = "Nickname";
    public static final String USER_PASSWORD_TAG= "Password";
    public static final String USER_EMAIL_TAG= "Email";
    public static final String USER_AVATAR_TAG= "Avatar";
    public static final String USER_DATEOFBIRTH_TAG= "Dateofbirth"; 
    
    public static final String ERROR_USER_NAME_TAG = "Error loading User from XML - Error in the attribute " + USER_NAME_TAG + " of the XML tag";
    public static final String ERROR_USER_FIRSTLASTNAME_TAG = "Error loading User from XML - Error in the attribute " + USER_FIRSTLASTNAME_TAG + " of the XML tag";
    public static final String ERROR_USER_SECONDLASTNAME_TAG = "Error loading User from XML - Error in the attribute " + USER_SECONDLASTNAME_TAG + " of the XML tag";
    public static final String ERROR_USER_NICKNAME_TAG = "Error loading User from XML - Error in the attribute " + USER_NICKNAME_TAG + " of the XML tag";
    public static final String ERROR_USER_PASSWORD_TAG = "Error loading User from XML - Error in the attribute " + USER_PASSWORD_TAG + " of the XML tag";
    public static final String ERROR_USER_EMAIL_TAG = "Error loading User from XML - Error in the attribute " + USER_EMAIL_TAG + " of the XML tag";
    public static final String ERROR_USER_AVATAR_TAG = "Error loading User from XML - Error in the attribute " + USER_AVATAR_TAG + " of the XML tag";
    public static final String ERROR_USER_DATEOFBIRTH_TAG = "Error loading User from XML - Error in the attribute " + USER_DATEOFBIRTH_TAG + " of the XML tag";
   
   

    public static final String ERROR_XML_EMPTY_FILE = "Error loading XML file - The file is empty";
    public static final String ERROR_XML_PROCESSING_FILE = "Error loading XML file - It's not possible processing the file";
    public static final String ERROR_XML_PROFESSOR_ID_NOT_EXIST = "Error loading XML file - The professor don't exist";
    
    
    public static final String USERS_XML_PATH = "src/model/users.xml";
}
    
