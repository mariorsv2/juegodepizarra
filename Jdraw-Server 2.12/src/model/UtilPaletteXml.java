/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ryu Alejo
 */
public class UtilPaletteXml 
{
    public static final String PALETTE_TAG = "Palette";
    public static final String PALETTE_USERNICKNAME_TAG = "UserNickname";
    public static final String PALETTE_PALETTENAME_TAG = "PaletteName";

    
    public static final String ERROR_PALETTE_USERNICKNAME_TAG = "Error loading Palette Data from XML - Error in the attribute " + PALETTE_USERNICKNAME_TAG + " of the XML tag";
    public static final String ERROR_PALETTE_PALETTENAME_TAG = "Error loading Palette Data from XML - Error in the attribute " + PALETTE_PALETTENAME_TAG + " of the XML tag";

   
   

    public static final String ERROR_XML_EMPTY_FILE = "Error loading XML file - The file is empty";
    public static final String ERROR_XML_PROCESSING_FILE = "Error loading XML file - It's not possible processing the file";
    public static final String ERROR_XML_PROFESSOR_ID_NOT_EXIST = "Error loading XML file - The professor don't exist";
    
    
    public static final String PALETTE_XML_PATH = "src/model/palettes.xml";
}
