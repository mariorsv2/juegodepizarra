/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author user
 */
public class RequestPlayXMLFile {
    public static String readWordUsersFromDataBase(String nicknameuser, String nicknamefriend)
    {
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname, nicknameuserfriend;
        String          word = "";
        boolean         found=false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilFriendsXML.FRIENDS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilFriendsXML.FRIENDNICKNAME_TAG);
                nicknameuserfriend       = child.getAttributeValue(UtilFriendsXML.FRIENDNICKNAMEFRIEND_TAG);

                if( nickname!= null      && nickname.equals(nicknameuser) &&
                    nicknameuserfriend  != null     && nicknamefriend.equals(nicknamefriend))
                { 
                    word                  =child.getAttributeValue(UtilRequestPlayXMLFile.WORDTOPLAY_TAG);
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(UtilFriendsXML.FRIENDNICKNAME_TAG);

                    if (nicknameuserfriend == null)
                        System.out.println(UtilFriendsXML.FRIENDNICKNAMEFRIEND_TAG);

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return word;
    }
    /**Method to Read AllRequestPlay From XMLfile
 * @param listrequest
 * @param nicknameuser 
 * @author user
 */
    public static void readAllRequestPlayFromJDrawDataBase(ListRequestPlay listrequest,String nicknameuser)
    {
        User            user = null;
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,nicknamefriend;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilRequestPlayXMLFile.REQUESTTOPLAY_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);
              
                nickname            = child.getAttributeValue(UtilRequestPlayXMLFile.FRIENDNICKNAME_TAG);
                nicknamefriend        = child.getAttributeValue(UtilRequestPlayXMLFile.FRIENDNICKNAMEFRIENDREQUEST_TAG);
                

                if((nickname!=null && nickname.equals(nicknameuser)))
                {
                    user=UserXmlFile.readUserFromJDrawDataBase(nicknamefriend);
                    System.out.println("Name: " + user.getName());
                    System.out.println("First lastname: " + user.getFirstLastName());
                    System.out.println("Second lastname: " + user.getSecondLastName());
                    listrequest.addFriend(user);
                }
                
                else
                {
                    if (nicknamefriend == null)
                        System.out.println(UtilRequestPlayXMLFile.ERROR_FRIEND_NICKNAMEFRIENDREQUEST_TAG);
                    if (nickname == null )
                        System.out.println(UtilRequestPlayXMLFile.ERROR_FRIEND_NICKNAME_TAG);
                }
                
                pos++;
            }
        }
              catch(JDOMParseException e)
        {
            System.out.println(UtilRequestPlayXMLFile.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilRequestPlayXMLFile.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilRequestPlayXMLFile.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (ParseException ex) {
            Logger.getLogger(FriendsXMLFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
 /**Method to Save RequesttoPlay From XMLfile
  * @param nickname
  * @param nicknamefriend 
  * @param word
 * @return boolean 
 * @author user
 */
    public static boolean saveRequesttoPlayInJDrawDataBase (String nickname, String nicknamefriend , String word)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilRequestPlayXMLFile.REQUESTTOPLAY_XML_PATH);

            root = doc.getRootElement();


            newChild = new Element(UtilRequestPlayXMLFile.FRIENDREQUESTTOPLAY_TAG);

  
            newChild.setAttribute(UtilRequestPlayXMLFile.FRIENDNICKNAME_TAG, nicknamefriend);
            newChild.setAttribute(UtilRequestPlayXMLFile.FRIENDNICKNAMEFRIENDREQUEST_TAG, nickname);
            newChild.setAttribute(UtilRequestPlayXMLFile.WORDTOPLAY_TAG, word);


            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilRequestPlayXMLFile.REQUESTTOPLAY_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
             catch(JDOMParseException e)
        {
            System.out.println(UtilRequestPlayXMLFile.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilRequestPlayXMLFile.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilRequestPlayXMLFile.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
}
