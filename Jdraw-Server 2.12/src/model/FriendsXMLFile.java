/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import model.User;
import model.UserXmlFile;

/**Class for Manage FriendXMLFile in the system
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 *
 *
 */
public class FriendsXMLFile {
    
 /**Method to Read Nickname Users From XMLfile
  * @param nicknameuser
  * @param nicknamefriend
 * @return boolean
 * @author user
 */
    public static boolean readNicknameUsersFromDataBase(String nicknameuser, String nicknamefriend)
    {
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname, nicknameuserfriend;
        boolean         found = false;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilFriendsXML.FRIENDS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);

                nickname         = child.getAttributeValue(UtilFriendsXML.FRIENDNICKNAME_TAG);
                nicknameuserfriend       = child.getAttributeValue(UtilFriendsXML.FRIENDNICKNAMEFRIEND_TAG);
                

                if( nickname!= null      && nickname.equals(nicknameuser) &&
                    nicknameuserfriend  != null     && nicknamefriend.equals(nicknamefriend))
                {
                    found = true;
                }
                else
                {
                    if (nickname == null)
                        System.out.println(UtilFriendsXML.FRIENDNICKNAME_TAG);

                    if (nicknameuserfriend == null)
                        System.out.println(UtilFriendsXML.FRIENDNICKNAMEFRIEND_TAG);

                    
                    pos++;
                }
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return found;
    }
 /**Method to Read AllFriends From XMLfile
 * @param listfriends
 * @param nicknameuser 
 * @author user
 */
    public static void readAllFriendsFromJDrawDataBase(ListFriends listfriends,String nicknameuser)
    {
        User            user = null;
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          nickname,nicknamefriend;
        int             pos = 0;

        SAXBuilder      builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilFriendsXML.FRIENDS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);
              
                nickname            = child.getAttributeValue(UtilFriendsXML.FRIENDNICKNAME_TAG);
                nicknamefriend        = child.getAttributeValue(UtilFriendsXML.FRIENDNICKNAMEFRIEND_TAG);
                

                if((nickname!=null && nickname.equals(nicknameuser)))
                {
                    user=UserXmlFile.readUserFromJDrawDataBase(nicknamefriend);
                    System.out.println("Name: " + user.getName());
                    System.out.println("First lastname: " + user.getFirstLastName());
                    System.out.println("Second lastname: " + user.getSecondLastName());
                    listfriends.addFriend(user);
                }
                 if((nicknamefriend!=null && nicknamefriend.equals(nicknameuser)))
                {
                    user=UserXmlFile.readUserFromJDrawDataBase(nickname);
                    System.out.println("Name: " + user.getName());
                    System.out.println("First lastname: " + user.getFirstLastName());
                    System.out.println("Second lastname: " + user.getSecondLastName());
                    listfriends.addFriend(user);
                }
                else
                {
                    if (nicknamefriend == null)
                        System.out.println(UtilFriendsXML.FRIENDNICKNAMEFRIEND_TAG);
                    if (nickname == null )
                        System.out.println(UtilFriendsXML.FRIENDNICKNAME_TAG);
                }
                
                pos++;
            }
        }
              catch(JDOMParseException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (ParseException ex) {
            Logger.getLogger(FriendsXMLFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
 /**Method to Save Friendship From XMLfile
  * @param nickname
  * @param nicknamefriend 
 * @return boolean 
 * @author user
 */
    public static boolean saveFriendsUsersInJDrawDataBase (String nickname, String nicknamefriend)
    {
        Document    doc;
        Element     root, newChild;

        SAXBuilder  builder = new SAXBuilder();

        try
        {
            doc = builder.build(UtilFriendsXML.FRIENDS_XML_PATH);

            root = doc.getRootElement();


            newChild = new Element(UtilFriendsXML.FRIENDNICKNAME_TAG);

  
            newChild.setAttribute(UtilFriendsXML.FRIENDNICKNAME_TAG, nickname);
            newChild.setAttribute(UtilFriendsXML.FRIENDNICKNAMEFRIEND_TAG, nicknamefriend);


            root.addContent(newChild);

            try
            {
                Format format = Format.getPrettyFormat();

              
                XMLOutputter out = new XMLOutputter(format);

         
                FileOutputStream file = new FileOutputStream(UtilFriendsXML.FRIENDS_XML_PATH);

     
                out.output(doc,file);

               
                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
             catch(JDOMParseException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(UtilFriendsXML.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }
}
