/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import model.FriendsXMLFile;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.FriendRequestsXMLFile;
import jsockets.server.logic.ServerLogic;
import jsockets.util.UtilFunctions;
import model.FirstLastNameComparator;
import model.ListColor;
import model.ListFriends;
import model.ListPalette;
import model.ListRequest;
import model.ListUser;
import model.Nicknamecomparator;
import model.PaletteXmlFile;
import model.PasswordComparator;
import model.RequestPlayXMLFile;
import model.User;
import model.StandardObject;
import model.Store;
import model.StoreXmlFile;
import model.UserXmlFile;
import model.UtilStoreXml;
/**
 * Class for Manage Client to server and Servertoclient in the system
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */

public class ManagementApplication implements ServerLogic
{
    private int operation;
/**
 * Method to execute the operation from client
 * @param Object 
 * @return answer 
 * @author user
 */
    @Override
    public byte[] executeOperation(Object arg)
    {
        try {
            byte [] answer,answer2;
            answer = null;
            answer2= null;
            
             StandardObject objectFromClient = (StandardObject) arg;
            
            
            String[] protocolo = objectFromClient.getProtocol().toString().split(":");
               
             
            if (UtilFunctions.isNumeric(protocolo[0])) {
                System.out.println("Username: " + protocolo[0]);
                this.operation = Integer.parseInt(protocolo[0]);
            } else {
                throw new RuntimeException("Error. La operacion solicitada no se encuentra soportada");
            }
            
            switch (this.operation)
            {
                case 1:
                    System.out.println("User logged in");
                    System.out.println("Username: " + protocolo[1]);
                    System.out.println("Password: " + protocolo[2]);
                    
                    
                    boolean isInTheSystem = UserXmlFile.readLogInFromDataBase(protocolo[1], protocolo[2]);
                    
                    if (isInTheSystem == true)
                    {
                        //devolver TRUE (string)
                        answer = UtilFunctions.stringToByteArray("TRUE");
                    }
                    else
                        answer = UtilFunctions.stringToByteArray("FALSE");
                                    
                    break;
                
                case 2:
                     System.out.println("User registed");
                    
                    User newuser = (User) objectFromClient.getObject();
                    int coins = 10;
                    int bombs = 5;
                    String defaultCoins = Integer.toString(coins);
                    String defaultBombs = Integer.toString(bombs);
                    if (UserXmlFile.readNicknameinJDrawFromDataBase(newuser.getNickname())== false){
                    System.out.println("Name: " + newuser.getName());
                    System.out.println("First lastname: " + newuser.getFirstLastName());
                    System.out.println("Second lastname: " + newuser.getSecondLastName());
                    System.out.println("E-mail: " + newuser.getEmail());
                    System.out.println("Date of birth: " + newuser.getBirthdate());
                    System.out.println("Nickname: " + newuser.getNickname());
                    System.out.println("Password: " + newuser.getPassword());
                    UserXmlFile.saveUserInJDrawDataBase(newuser);
                    StoreXmlFile.saveUserDataStoreInDataBase(newuser.getNickname(),defaultCoins,defaultBombs);
                    answer = UtilFunctions.stringToByteArray("TRUE");
                    answer2=UtilFunctions.fileToByteArray(newuser.getAvatar());
                   try {
                      UtilFunctions.createFileFromByteArray(answer2,"src/Imageuser/", newuser.getNickname(),UtilFunctions.getImageType(newuser.getAvatar()), true);
                     } catch (Exception e) {
                    
                        }
                    }
                    else 
                        answer = UtilFunctions.stringToByteArray("FALSE");
                    break;
                    
                case 3:
                    ListUser la = new ListUser();
                    UserXmlFile.readAllUsersFromJDrawDataBase(la);
                    
                    answer = UtilFunctions.objectToByteArray(la);
                    break;
                    
                 case 4:
                    ListFriends lf = new ListFriends();
                    System.out.println("Nickname: " + protocolo[1]);
                    FriendsXMLFile.readAllFriendsFromJDrawDataBase(lf, protocolo[1]);
                    
                    answer = UtilFunctions.objectToByteArray(lf);
                    break;
                     
                 case 5:
                   
                    System.out.println("Nickname: " + protocolo[1]);
                   
                 
                    User user= UserXmlFile.readUserFromJDrawDataBase(protocolo[1]);
                   
                    
                    
                    if (user!=null)
                    {

                        System.out.println("Name: " + user.getName());//
                        System.out.println("First lastname: " + user.getFirstLastName());
                        System.out.println("Second lastname: " + user.getSecondLastName());
                        answer = UtilFunctions.objectToByteArray(user);
                        
                    }
                    else
                        answer = UtilFunctions.objectToByteArray(user);
                
                    break;
                     
                     
                case 6:
                    
                     System.out.println("User logged in");
                     System.out.println("Username: " + protocolo[1]);
                     System.out.println("Password: " + protocolo[2]);
                     FriendRequestsXMLFile.saveRequestUserInJDrawDataBase(protocolo[1],protocolo[2]);
                      answer=UtilFunctions.stringToByteArray("TRUE");
                    break;
                    
                 case 7:
                     System.out.println("User logged in");
                     System.out.println("Username: " + protocolo[1]);
                     ListRequest list = new ListRequest();
                     FriendRequestsXMLFile.readAllFriendsFromJDrawDataBase(list, protocolo[1]);
                     answer = UtilFunctions.objectToByteArray(list);

                    break;
                     
                     
                  case 8:
                     System.out.println("User logged in");
                     System.out.println("Username: " + protocolo[1]);
                     System.out.println("Username: " + protocolo[2]);
                     System.out.println("Username: " + protocolo[3]);
                     if (protocolo[3].compareTo("TRUE")== 0){
                         FriendsXMLFile.saveFriendsUsersInJDrawDataBase(protocolo[1], protocolo[2]);
                         FriendRequestsXMLFile.removeRequestUserFromJDrawDataBase(protocolo[1], protocolo[2]);
                         
                         answer=UtilFunctions.stringToByteArray("TRUE");
                     }
                     else{
                         FriendRequestsXMLFile.removeRequestUserFromJDrawDataBase(protocolo[1], protocolo[2]);
                         answer=UtilFunctions.stringToByteArray("TRUE");}
                     break;
                   
                   case 9:
                         System.out.println("User registed");
                        //Save user in xml file
                        User updateUser = (User) objectFromClient.getObject();
                        if (UserXmlFile.readNicknameinJDrawFromDataBase(updateUser.getNickname())== true){
                        System.out.println("Name: " + updateUser.getName());
                        System.out.println("First lastname: " + updateUser.getFirstLastName());
                        System.out.println("Second lastname: " + updateUser.getSecondLastName());
                        System.out.println("E-mail: " + updateUser.getEmail());
                        System.out.println("Date of birth: " + updateUser.getBirthdate());
                        System.out.println("Nickname: " + updateUser.getNickname());
                        System.out.println("Password: " + updateUser.getPassword());
                        UserXmlFile.removeUserFromJDrawDataBase(updateUser.getNickname());
                        UserXmlFile.updateUserInJDrawDataBase(updateUser);
                        answer = UtilFunctions.stringToByteArray("TRUE");
                        //answer2=UtilFunctions.fileToByteArray(updateUser.getAvatar());
                        }
                        else    
                            answer = UtilFunctions.stringToByteArray("FALSE");
                        break;
                    case 11:
                    System.out.println("User logged in");
                    System.out.println("Username: " + protocolo[1]);
                    boolean save= RequestPlayXMLFile.saveRequesttoPlayInJDrawDataBase(protocolo[1], protocolo[2], protocolo[3]);
                    if (save== true){
                       answer = UtilFunctions.stringToByteArray("TRUE");
                       
                    }
                    else 
                        answer = UtilFunctions.stringToByteArray("FALSE");
                        break;
                   case 14:
                       answer2= (byte [])objectFromClient.getObject();
                   try {
                      UtilFunctions.createFileFromByteArray(answer2,"src/ImageUser/", protocolo[1],"jpg", true);
                     } catch (Exception e) {
                    
                        }
                        answer = UtilFunctions.stringToByteArray("TRUE");
                        break;
                   case 15:
                       System.out.println("Username: " + protocolo[1]);
                       String bomb=protocolo[2];
                       String nickname = protocolo[1];
                       String price = protocolo[3];
                       if (StoreXmlFile.readNicknameinStoreJDrawFromDataBase(nickname)==true)
                       {
                           String oldBombs=StoreXmlFile.readBombsFromStoreDataBase(nickname);
                           String coinsjdraw= StoreXmlFile.readCoinsFromStoreDataBase(nickname);
                           int coinsToPay = Integer.parseInt(price);
                           int actualCoins = Integer.parseInt(coinsjdraw);
                           if (coinsToPay <= actualCoins)
                           {
                               System.out.println(coinsjdraw);
                               int contbombs=Integer.parseInt(oldBombs);
                               int contbombs2=Integer.parseInt(bomb);
                               int contbombstotal=contbombs+contbombs2;
                               int newCoins = actualCoins - coinsToPay;
                               String updateCoins = Integer.toString(newCoins);
                               String finalbombs=Integer.toString(contbombstotal);
                               StoreXmlFile.removeUserFromStoreDataBase(nickname);
                               System.out.println(finalbombs);
                               System.out.println(updateCoins);
                               if (StoreXmlFile.saveNewBombsInDataBase(nickname, finalbombs, updateCoins)==true)
                               {
                                   answer = UtilFunctions.stringToByteArray("TRUE");
                               }
                           }
                           else
                           {
                               answer = UtilFunctions.stringToByteArray("FALSE");
                           }
                           
                       }  
                       
                       break;
                      
                   case 16:
                    System.out.println("User logged in");
                    System.out.println("Username: " + protocolo[1]);
                 
                    
                    //Proccess user in xml file
                    boolean isInTheSystemOut = UserXmlFile.readNicknameinJDrawFromDataBase(protocolo[1]);
                    
                    if (isInTheSystemOut == true)
                    {
                        //devolver TRUE (string)
                        answer = UtilFunctions.stringToByteArray("TRUE");
                    }
                    else
                        answer = UtilFunctions.stringToByteArray("FALSE");
                                    
                    break;
                       
                case 19:
                    
                     System.out.println("Username: " + protocolo[1]);

                    String path="src/ImageUser"+"/"+ protocolo[1]+".jpg";

                    System.out.println("Nickname: " + path);
                    answer = UtilFunctions.fileToByteArray(path);
                                    
                    break;
                 case 20:
                    
                       answer2= (byte [])objectFromClient.getObject();
                   try {
                      UtilFunctions.createFileFromByteArray(answer2,"src/ImageJdraw/", protocolo[1]+protocolo[2]+protocolo[3],"jpg", true);
                     } catch (Exception e) {
                    
                        }
                        answer = UtilFunctions.stringToByteArray("TRUE");
                        break;
                    
                case 21:
                    
                    System.out.println("Username: " + protocolo[1]);
                    String cost=protocolo[2];
                    String paletteName = protocolo[3];
                    if (StoreXmlFile.readNicknameinStoreJDrawFromDataBase(protocolo[1])==true)
                    {
                        String coinsjdraw= StoreXmlFile.readCoinsFromStoreDataBase(protocolo[1]);
                        int coinsToPay = Integer.parseInt(cost);
                        int actualCoins = Integer.parseInt(coinsjdraw);
                        String paletteold=PaletteXmlFile.readPalettesFromStoreDataBase(protocolo[1]);
                        if (actualCoins==0)
                        {
                            answer = UtilFunctions.stringToByteArray("FALSE");
                        }
                        else
                        if ((paletteold.compareTo("")!=0))
                        {
                             System.out.println("ENTRO");
                            if((PaletteXmlFile.searchPurchasedPalettesFromStoreDataBase(protocolo[1], paletteName)==true))
                            {
                                answer = UtilFunctions.stringToByteArray("FALSE");
                                return answer;
                            }
                        }
                        else
                        if ((coinsToPay <= actualCoins))
                        {
                            
                            int newCoins = actualCoins - coinsToPay;
                            String updateCoins = Integer.toString(newCoins);
                            String userBombs = StoreXmlFile.readBombsFromStoreDataBase(protocolo[1]);
                            StoreXmlFile.removeUserFromStoreDataBase(protocolo[1]);
                            System.out.println(paletteName+"1"+paletteold);
                            if (StoreXmlFile.saveUserDataStoreInDataBase(protocolo[1], userBombs, updateCoins)==true){
                            //String paletteold=PaletteXmlFile.readPalettesFromStoreDataBase(protocolo[1]);
                                PaletteXmlFile.removeUserFromPaletteDataBase(protocolo[1]);
                            if (paletteold.compareTo("")==0){
                                if (PaletteXmlFile.saveNewPaletteInDataBase(protocolo[1], paletteName)==true)
                            {
                                answer = UtilFunctions.stringToByteArray("TRUE");
                            }
                            else
                            {
                                answer = UtilFunctions.stringToByteArray("FALSE");
                            }
                            }
                            else
                                //PaletteXmlFile.removeUserFromPaletteDataBase(protocolo[1]);
                             if (PaletteXmlFile.saveNewPaletteInDataBase(protocolo[1], paletteName+","+paletteold)==true)
                            {
                                answer = UtilFunctions.stringToByteArray("TRUE");
                            }
                            else
                            {
                                answer = UtilFunctions.stringToByteArray("FALSE");
                            }
                            }
                        }
                    }
                    
                    break;
                case 22:
                    
                    ListColor lc = new ListColor();
                    System.out.println("Nickname: " + protocolo[1]);
                    //ColorXMLFile.readAllColorFromJDrawDataBase(lc, protocolo[1]);
                    
                    answer = UtilFunctions.objectToByteArray(lc);
                    break;
                case 23:
                    System.out.println("Username: " + protocolo[1]);
                    String palette = PaletteXmlFile.readPalettesFromStoreDataBase(protocolo[1]);
                    answer = UtilFunctions.stringToByteArray(palette);
                    break; 
            }
            return answer;
        } catch (ParseException ex) {
            Logger.getLogger(ManagementApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
