/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import jsockets.server.logic.Server;

/**Class for Main in server
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 *
 * 
 */

public class Main 
{
/**Method for parameters for server
 * @param args
 */
    public static void main(String [] args)
    {
        
        String[] parameters = {"7687", "controller.ManagementApplication"};
        Server.main(parameters);
    }
}
