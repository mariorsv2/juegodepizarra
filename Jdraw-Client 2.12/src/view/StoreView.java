/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.StoreViewController;
import java.awt.Color;

/**
 *
 * @author Ryu Alejo
 */
public class StoreView extends MyOwnJFrame {

    /**
     * Creates new form StoreView
     */
    public StoreView() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jB3BombsPack = new javax.swing.JButton();
        jB5BombsPack = new javax.swing.JButton();
        jBFPastelsPack = new javax.swing.JButton();
        jBFluorecentPack = new javax.swing.JButton();
        jBBasicPack = new javax.swing.JButton();
        jBVintagePack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jB3BombsPack.setText("3 Bombs Pack");
        jB3BombsPack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB3BombsPackActionPerformed(evt);
            }
        });

        jB5BombsPack.setText("5 Bombs Pack");
        jB5BombsPack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB5BombsPackActionPerformed(evt);
            }
        });

        jBFPastelsPack.setText("Pastels Pack");
        jBFPastelsPack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFPastelsPackActionPerformed(evt);
            }
        });

        jBFluorecentPack.setText("Fluorcent Pack");
        jBFluorecentPack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFluorecentPackActionPerformed(evt);
            }
        });

        jBBasicPack.setText("Basic Pack");
        jBBasicPack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBasicPackActionPerformed(evt);
            }
        });

        jBVintagePack.setText("Vintage Pack");
        jBVintagePack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBVintagePackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jB5BombsPack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jB3BombsPack, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                    .addComponent(jBFPastelsPack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBFluorecentPack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(56, 56, 56)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jBBasicPack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBVintagePack, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE))
                .addContainerGap(156, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jBBasicPack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jB3BombsPack, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jB5BombsPack, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBVintagePack, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jBFPastelsPack, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBFluorecentPack, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(76, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(726, 399));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jB3BombsPackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB3BombsPackActionPerformed
        // TODO add your handling code here:
        int bombs = 3;
        int price = 5;
        StoreViewController.buyBombs(bombs,price);
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_jB3BombsPackActionPerformed

    private void jB5BombsPackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB5BombsPackActionPerformed
        // TODO add your handling code here:
        int bombs = 5;
        int price = 10;
        StoreViewController.buyBombs(bombs,price);
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_jB5BombsPackActionPerformed

    private void jBFPastelsPackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFPastelsPackActionPerformed
        // TODO add your handling code here:
        int price = 20;
        String paletteType ="PastelsPack";
        StoreViewController.buyColorPalette(price,paletteType);
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_jBFPastelsPackActionPerformed

    private void jBFluorecentPackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFluorecentPackActionPerformed
        // TODO add your handling code here:
        int price = 30;
        String paletteType = "FluorecentPack";
        StoreViewController.buyColorPalette(price,paletteType);
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_jBFluorecentPackActionPerformed

    private void jBBasicPackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBasicPackActionPerformed
        // TODO add your handling code here:
        int price = 10;
        String paletteType = "BasicPack";
        StoreViewController.buyColorPalette(price,paletteType);
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_jBBasicPackActionPerformed

    private void jBVintagePackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBVintagePackActionPerformed
        // TODO add your handling code here:
        int price = 40;
        String paletteType = "VintangePack";
        StoreViewController.buyColorPalette(price,paletteType);
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_jBVintagePackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StoreView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StoreView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StoreView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StoreView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StoreView().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jB3BombsPack;
    private javax.swing.JButton jB5BombsPack;
    private javax.swing.JButton jBBasicPack;
    private javax.swing.JButton jBFPastelsPack;
    private javax.swing.JButton jBFluorecentPack;
    private javax.swing.JButton jBVintagePack;
    // End of variables declaration//GEN-END:variables
}
