/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.LoginViewController;
import controller.SearchForFriendsViewController;
import java.awt.Color;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import jsockets.util.UtilFunctions;

/**
 *
 * @author Usuario
 */
public class SearchForFriendsView extends MyOwnJFrame {

    /**
     * Creates new form SearchForFriendsView
     */
    public SearchForFriendsView() {
        initComponents();
        myOwnComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        JTFNickname = new javax.swing.JTextField();
        jlSearch = new javax.swing.JLabel();
        JBSearch = new javax.swing.JButton();
        jLAvatar = new javax.swing.JLabel();
        jLNickname = new javax.swing.JLabel();
        jLNicknameField = new javax.swing.JLabel();
        jLFirstLastName = new javax.swing.JLabel();
        jLFirstLastNameField = new javax.swing.JLabel();
        jLSecondLastname = new javax.swing.JLabel();
        jLSecondLastNameField = new javax.swing.JLabel();
        jLName = new javax.swing.JLabel();
        jLNameField = new javax.swing.JLabel();
        jBOk = new javax.swing.JButton();
        jBCancel = new javax.swing.JButton();
        jBAvatar = new javax.swing.JButton();
        jLMessage = new javax.swing.JLabel();
        javax.swing.JLabel jLFIelds = new javax.swing.JLabel();
        jLSearchFriendsBackground = new javax.swing.JLabel();

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Search Friends");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        JTFNickname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTFNicknameActionPerformed(evt);
            }
        });
        getContentPane().add(JTFNickname, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, 135, 30));

        jlSearch.setText("(*)Introduce nickname to look for:");
        getContentPane().add(jlSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 24, 200, 20));

        JBSearch.setText("Search");
        JBSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSearchActionPerformed(evt);
            }
        });
        getContentPane().add(JBSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 20, 80, 30));
        getContentPane().add(jLAvatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 70, 108, 103));

        jLNickname.setForeground(new java.awt.Color(255, 255, 255));
        jLNickname.setText("NickName:");
        getContentPane().add(jLNickname, new org.netbeans.lib.awtextra.AbsoluteConstraints(47, 208, 80, 23));
        getContentPane().add(jLNicknameField, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 200, 150, 30));

        jLFirstLastName.setForeground(new java.awt.Color(255, 255, 255));
        jLFirstLastName.setText("FirstLastName:");
        getContentPane().add(jLFirstLastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(47, 243, 110, 30));
        getContentPane().add(jLFirstLastNameField, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 240, 227, 30));

        jLSecondLastname.setForeground(new java.awt.Color(255, 255, 255));
        jLSecondLastname.setText("SecondLastName:");
        getContentPane().add(jLSecondLastname, new org.netbeans.lib.awtextra.AbsoluteConstraints(47, 278, 110, 30));
        getContentPane().add(jLSecondLastNameField, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 280, 233, 26));

        jLName.setForeground(new java.awt.Color(255, 255, 255));
        jLName.setText("Name:");
        getContentPane().add(jLName, new org.netbeans.lib.awtextra.AbsoluteConstraints(47, 160, 60, 40));
        getContentPane().add(jLNameField, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 160, 140, 40));

        jBOk.setText("Send Request");
        jBOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBOkActionPerformed(evt);
            }
        });
        getContentPane().add(jBOk, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, 120, 40));

        jBCancel.setText("Cancel");
        jBCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelActionPerformed(evt);
            }
        });
        getContentPane().add(jBCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 370, 90, 40));

        jBAvatar.setText("AvatarFriend");
        jBAvatar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAvatarActionPerformed(evt);
            }
        });
        getContentPane().add(jBAvatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 370, 130, 40));

        jLMessage.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLMessage.setForeground(new java.awt.Color(0, 153, 51));
        jLMessage.setText("Some fields are empty");
        getContentPane().add(jLMessage, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 330, 140, -1));

        jLFIelds.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLFIelds.setForeground(new java.awt.Color(255, 255, 255));
        jLFIelds.setText("(*)Field Required");
        getContentPane().add(jLFIelds, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 310, 100, -1));

        jLSearchFriendsBackground.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/SearchFriendsBackground.jpg"))); // NOI18N
        getContentPane().add(jLSearchFriendsBackground, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 470, 420));

        setSize(new java.awt.Dimension(484, 488));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
  public void myOwnComponents(){
        markFields(false);
    }
       private boolean allfieldarenotempty(){
         if (this.JTFNickname.getText().equals(""))
             return false;
         return true;
           
     }
    public void markFields(boolean markempty){
         this.jLMessage.setForeground(Color.red);
         this.jlSearch.setForeground(Color.black);
         
         if (markempty){
             this.jLMessage.setVisible(true);
              if (JTFNickname.getText().equals(""))
                this.jlSearch.setForeground(Color.red);}
         else 
             this.jLMessage.setVisible(false);
       }
    private void JBSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSearchActionPerformed
       if (allfieldarenotempty()== false)
            markFields(true);
        else{
            SearchForFriendsViewController.initOutlets(jLNameField,jLFirstLastNameField,jLSecondLastNameField,jLAvatar,JTFNickname);
            if (JTFNickname.getText().compareTo(LoginViewController.jTFNickname.getText())!= 0){
            SearchForFriendsViewController.NicknameSearch(JTFNickname.getText());
            if (SearchForFriendsViewController.NicknameSearch(JTFNickname.getText())== true)
            {
              jLNicknameField.setText(JTFNickname.getText());
            }
            else
                JOptionPane.showMessageDialog(rootPane, "User is not in the system", "DataBase", WIDTH);
            }
            else 
                  JOptionPane.showMessageDialog(rootPane, "Can not send invitation yourself", "DataBase", WIDTH);
       }
    }//GEN-LAST:event_JBSearchActionPerformed

    private void jBCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelActionPerformed
        // TODO add your handling code here:
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_jBCancelActionPerformed

    private void jBOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBOkActionPerformed
        // TODO add your handling code here:
        if (allfieldarenotempty()== false)
            markFields(true);
        else{
        if (SearchForFriendsViewController.RequestInvitation()==true)
            JOptionPane.showMessageDialog(rootPane, "Request Send ", "Request", WIDTH);
        else
            JOptionPane.showMessageDialog(rootPane, "Error in Send Request ", "Request", WIDTH);
        }
    }//GEN-LAST:event_jBOkActionPerformed

    private void jBAvatarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAvatarActionPerformed
        // TODO add your handling code here:
       if (allfieldarenotempty()== false)
            markFields(true);
        else
           SearchForFriendsViewController.AvatarFriend(JTFNickname.getText());
        
    }//GEN-LAST:event_jBAvatarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        this.restoreFatherWindow();
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void JTFNicknameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTFNicknameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFNicknameActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SearchForFriendsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SearchForFriendsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SearchForFriendsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SearchForFriendsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SearchForFriendsView().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBSearch;
    private javax.swing.JTextField JTFNickname;
    private javax.swing.JButton jBAvatar;
    private javax.swing.JButton jBCancel;
    private javax.swing.JButton jBOk;
    private javax.swing.JLabel jLAvatar;
    private javax.swing.JLabel jLFirstLastName;
    private javax.swing.JLabel jLFirstLastNameField;
    private javax.swing.JLabel jLMessage;
    private javax.swing.JLabel jLName;
    private javax.swing.JLabel jLNameField;
    private javax.swing.JLabel jLNickname;
    private javax.swing.JLabel jLNicknameField;
    private javax.swing.JLabel jLSearchFriendsBackground;
    private javax.swing.JLabel jLSecondLastNameField;
    private javax.swing.JLabel jLSecondLastname;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel jlSearch;
    // End of variables declaration//GEN-END:variables
}
