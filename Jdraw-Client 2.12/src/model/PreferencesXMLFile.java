/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import model.Preferences;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**Class to manage the XML file
 *
 * @author user
 */
public class PreferencesXMLFile {
    
/**Method to read the preferences of the user
 * 
 * 
 */
    public static Preferences  readPreferencesFromUserDataBase() 
    {
        Preferences     preferences=null;
        Document        doc;
        Element         root,child;
        List <Element>  rootChildrens;
        String          userIp,userPort, Folderuser;
   
        
        int             pos = 0;
        SAXBuilder      builder = new SAXBuilder();
       
        try
        {
            doc = builder.build(Util.PREFERENCES_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size())
            {
                child = rootChildrens.get(pos);
                
                userIp      = child.getAttributeValue(Util.USERIP_TAG);
                userPort          = child.getAttributeValue(Util.USERPORT_TAG);
                Folderuser          = child.getAttributeValue(Util.FOLDERUSER_TAG);
                
              
               
                
                if(userIp != null &&  
                  userPort != null && 
                  Folderuser!= null  )
                   
                {
                    int portuser=Integer.parseInt(userPort);
                    preferences = new Preferences(userIp,portuser,Folderuser);
               
                    }
                else
                {
                 
                    
                    if (userIp == null)
                        System.out.println(Util.ERROR_PREFERENCES_USERIP_TAG);

                    if (userPort == null)
                        System.out.println(Util.ERROR_PREFERENCES_USERPORT_TAG);
                    
                    if (Folderuser == null)
                        System.out.println(Util.ERROR_PREFERENCES_FOLDERUSER_TAG);

                   
                    
                    
                }
                
               pos++; 
                
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        return preferences;
        
    }
/**Method to save preferences of the user in the XML file
 * 
 * @param preferences
 * @return boolean
 */
    public static boolean savePreferencesInJdrawDataBase(Preferences preferences)
    {
        Document    doc;
        Element     root, newChild;
      
        SAXBuilder  builder = new SAXBuilder();
        try
        {
            doc = builder.build(Util.PREFERENCES_XML_PATH); 
            root = doc.getRootElement();
            root.removeContent();
            
            newChild = new Element(Util.PREFERENCES_TAG);       
            newChild.setAttribute(Util.USERIP_TAG, preferences.getUserip());
            newChild.setAttribute(Util.USERPORT_TAG, Integer.toString(preferences.getUserport()));
            newChild.setAttribute(Util.FOLDERUSER_TAG, preferences.getFolderpath());
           
            
            
            root.addContent(newChild);
            
            try
            {
                Format format = Format.getPrettyFormat();

                XMLOutputter out = new XMLOutputter(format);

                FileOutputStream file = new FileOutputStream(Util.PREFERENCES_XML_PATH);

                out.output(doc,file);

                file.flush();
                file.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        catch(JDOMParseException e)
        {
            System.out.println(Util.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        }
        catch(JDOMException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println(Util.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return true;
    }




}
