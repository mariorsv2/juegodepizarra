/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import model.FirstLastNameComparator;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/**Class for the list of all users in the system
 * 
 * @author Ryu Alejo
 */
public class ListUser implements Serializable
{
    SortedSet <User> listUser;
    Comparator sortedComparator; 
    /**Method to initialize ListUser and Comparator
     * 
     * @return iterator of users
     */
    public ListUser()
    {
        sortedComparator = new FirstLastNameComparator();
        this.listUser = new TreeSet(sortedComparator);
    }
    /**Method to go over the list of users
     * 
     * @return iterator of users
     */
    public Iterator iterator()
    {
        return this.listUser.iterator();
    }
     /**Method to add a user to the list of users
     *  @param user 
     * @return boolean
     */
    public boolean addUser(User user)
    {
    return this.listUser.add(user);
    }
    /**Method to remove a user to the list of users
     * @param user
     * @return boolean 
     */
    public boolean removeUser(User user)
    {
        return this.listUser.remove(user);
    }
    /**Method to search a user to the list of users
     * @param nicknameuser
     * @return User
     */
    public User  searchUser(String nicknameuser)
    {
         Iterator    iterator;
        User     actualUser=null;
        
        iterator = this.listUser.iterator();
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            if (actualUser.getNickname().equals(nicknameuser)){
                return actualUser;
            }
        }
        return actualUser;
    }
    /**Method to print the list of users
     * 
     * 
     */
    public void printUsers()
    {
        Iterator    iterator;
        User     actualUser;
        
        iterator = this.listUser.iterator();
        
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            System.out.println("name : " + actualUser.getName());
        
    }
}
}
