/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**Class to content all constructors and getter and setter of the class
 *
 * @author user
 */
public class Preferences {
    private String userip;
    private int userport;
    private String folderpath;
 /**Method to initializes parameters
 * @param userip 
 * @param userport 
 * @param folderpath 
 * 
 */  
    public Preferences(String userip, int userport, String folderpath) {
        this.userip = userip;
        this.userport = userport;
        this.folderpath = folderpath;
    }
/**Method to get the user IP
 * 
 * @return userip
 */
    public String getUserip() {
        return userip;
    }
/**Method to set the User IP
 * 
 * @param userip 
 */
    public void setUserip(String userip) {
        this.userip = userip;
    }
/**Method to get the User Port
 * 
 * @return userport
 */
    public int getUserport() {
        return userport;
    }
/**Method to set the User Port
 * 
 * @param userport 
 */
    public void setUserport(int userport) {
        this.userport = userport;
    }
/**Method to get the Folder Path
 * 
 * @return folderpath
 */
    public String getFolderpath() {
        return folderpath;
    }
/**Method to set the Folder Path
 * 
 * @param folderpath 
 */
    public void setFolderpath(String folderpath) {
        this.folderpath = folderpath;
    }   
    
}
