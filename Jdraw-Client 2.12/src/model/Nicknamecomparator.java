/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Comparator;

/**Class to compare nicknames of users
 *
 * @author user
 */
public class Nicknamecomparator implements Comparator
{
/**Method that returns the largest nickname
 * @param firstObject
 * @param secondObject 
 * @return int
 * @author user
 */
    @Override

    public int compare(Object firstObject, Object secondObject)
    {
        int result = ((User) firstObject).getNickname().compareTo(((User) secondObject).getNickname());
        
        if (result <= 0)
            return -1;
        
        return 1;
    }
}
    

