/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author user
 */
public class ListRequestPlay implements Serializable
{
    SortedSet <User> listrequestplay;
    Comparator sortedComparator; 
            
    /**Method to initializes ListRequest and Comparator
     * 
     * 
     */
     
    public ListRequestPlay()
    {
        sortedComparator = new FirstLastNameComparator();
        this.listrequestplay = new TreeSet(sortedComparator);
    }
    /**Method to go over the list of requests
     * 
     * @return iterator of requests
     */
    public Iterator iterator()
    {
        return this.listrequestplay.iterator();
    }
    /**Method to add a friend to the list of requests
     * @param user
     * @return boolean if the request was added
     */
    public boolean addFriend(User user)
    {
    return this.listrequestplay.add(user);
    }
   /**Method to remove a friend to the list of requests
     * @param user
     * @return boolean if the request was remove
     */
    public boolean removeFriend(User user)
    {
        return this.listrequestplay.remove(user);
    }

        /**Method to search in the List of Request
     * @param nicknameuser 
     * @return User
     */
      public User  searchfriend(String nicknameuser)
    {
         Iterator    iterator;
        User     actualUser=null;
        
        iterator = this.listrequestplay.iterator();
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            if (actualUser.getNickname().equals(nicknameuser)){
                return actualUser;
            }
        }
        return actualUser;
    }
      
       /**Method to print the list of requests
     * 
     * @return the list of requests
     */
    public void printAllFriends()
    {
        Iterator    iterator;
        User     actualUser;
        
        iterator = this.listrequestplay.iterator();
        
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            System.out.println(actualUser);
        }
    }
    
}
