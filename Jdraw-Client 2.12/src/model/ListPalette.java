/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Ryu Alejo
 */
public class ListPalette implements Serializable
{
    
    private List <ListColor> listPalette;
    
    public ListPalette()
    {
        this.listPalette = new ArrayList();
    }
    
    public Iterator iterator()
    {
        return this.listPalette.iterator();
    }
    
    public boolean addCPalette(ListColor list)
    {
        return this.listPalette.add(list);
    }
    
    public boolean removePalette(ListColor list)
    {
        return this.listPalette.remove(list);
    }
    
    public void printAllColors()
    {
        Iterator    iterator;
        ListColor     actualList;
        
        iterator = this.listPalette.iterator();
        
        while (iterator.hasNext())
        {
            System.out.println("hola");
            actualList = (ListColor) iterator.next();
            String colores=actualList.toString();
            Color colorName = Color.decode(colores);
            System.out.println(colorName);
        }
    }
    
//    public Color getColors()
//    {
//        
//        Iterator    iterator;
//        Color     actualColor;
//        actualColor = null;
//        iterator = this.listPalette.iterator();
//        
//        while (iterator.hasNext())
//        {
//            System.out.println("entre");
//            actualColor = (Color) iterator.next();
//            return actualColor;
//        }
//        return actualColor;
//        
//    }
}
