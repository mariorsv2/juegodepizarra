/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/** Class to handle the List of Friends
 *
 * @author user
 */
public class ListFriends implements Serializable {
    SortedSet <User> listFriends;
    Comparator sortedComparator; 
     /**Method to initializes Listfriends and Comparator
     * 
     * 
     */
    
    public ListFriends()
    {
        sortedComparator = new FirstLastNameComparator();
        this.listFriends = new TreeSet(sortedComparator);
    }
    /**Method to go over the list of friends
     * 
     * @return iterator of friends
     */
    public Iterator iterator()
    {
        return this.listFriends.iterator();
    }
     /**Method to add a friend to the list of friends
     * @param user
     * @return boolean
     */
    
    public boolean addFriend(User user)
    {
    return this.listFriends.add(user);
    }
    /**Method to remove a friend to the list of friends
     * @param user 
     * @return boolean
     */
    public boolean removeFriend(User user)
    {
        return this.listFriends.remove(user);
    }
        /**Method to search in the List of Friends
     * @param nicknameuser
     * @return User
     */
      public User  searchfriend(String nicknameuser)
    {
         Iterator    iterator;
        User     actualUser=null;
        
        iterator = this.listFriends.iterator();
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            if (actualUser.getNickname().equals(nicknameuser)){
                return actualUser;
            }
        }
        return actualUser;
    }
    /**Method to print the list of friends
     * 
     * 
     */
    public void printAllFriends()
    {
        Iterator    iterator;
        User     actualUser;
        
        iterator = this.listFriends.iterator();
        
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            System.out.println(actualUser);
        }
    }
}
    
    

