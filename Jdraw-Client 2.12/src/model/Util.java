/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**Class For initializes parameters for UserXmlFile
 *
 * @author user
 */
public class Util {
    public static final String PREFERENCES_TAG = "Preferences";
    public static final String USERIP_TAG = "UserIp";
    public static final String USERPORT_TAG = "UserPort";
    public static final String FOLDERUSER_TAG = "UserFolder";
   
    
    public static final String ERROR_PREFERENCES_USERIP_TAG = "Error loading User from XML - Error in the attribute " + USERIP_TAG + " of the XML tag";
    public static final String ERROR_PREFERENCES_USERPORT_TAG = "Error loading User from XML - Error in the attribute " + USERPORT_TAG + " of the XML tag";
    public static final String ERROR_PREFERENCES_FOLDERUSER_TAG = "Error loading User from XML - Error in the attribute " + FOLDERUSER_TAG + " of the XML tag";
    
    public static final String ERROR_XML_EMPTY_FILE = "Error loading XML file - The file is empty";
    public static final String ERROR_XML_PROCESSING_FILE = "Error loading XML file - It's not possible processing the file";
    
    public static final String PREFERENCES_XML_PATH = "src/Model/PreferencesXML.xml";
}
