/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/**Class to handle the list of requests
 *
 * @author user
 */
public class ListRequest implements Serializable
{
    SortedSet <User> listrequest;
    Comparator sortedComparator; 
         /**Method to initializes ListRequest
     * 
     * 
     */
    
    public ListRequest()
    {
        sortedComparator = new FirstLastNameComparator();
        this.listrequest = new TreeSet(sortedComparator);
    }
    /**Method to go over the list of requests
     * 
     * @return iterator of requests
     */
    public Iterator iterator()
    {
        return this.listrequest.iterator();
    }
    /**Method to add a friend to the list of requests
     * @param user 
     * @return boolean 
     */
    public boolean addFriend(User user)
    {
    return this.listrequest.add(user);
    }
   /**Method to remove a friend to the list of requests
     * @param user 
     * @return boolean
     */
    public boolean removeFriend(User user)
    {
        return this.listrequest.remove(user);
    }
    /**Method to search in the List of Request
     * @param nicknameuser
     * @return User
     */
      public User  searchfriend(String nicknameuser)
    {
         Iterator    iterator;
        User     actualUser=null;
        
        iterator = this.listrequest.iterator();
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            if (actualUser.getNickname().equals(nicknameuser)){
                return actualUser;
            }
        }
        return actualUser;
    }
     /**Method to print the list of requests
     * 
     * 
     */
    public void printAllFriends()
    {
        Iterator    iterator;
        User     actualUser;
        
        iterator = this.listrequest.iterator();
        
        while (iterator.hasNext())
        {
            actualUser = (User) iterator.next();
            System.out.println(actualUser);
        }
    }
}


    

