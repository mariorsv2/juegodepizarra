/*
 * This class belong to the data of each user in the system
 * Name, first last name, second last name, nickname, password and avatar
 * 
 */
package model;

import java.util.Date;
import java.io.Serializable;
/**Class to content all constructors and getter and setter of the class
 * 
 * @author Ryu Alejo
 */
public class User implements Serializable
{
    private String name;
    private String firstLastName;
    private String secondLastName;
    private Date birthdate;
    private String nickname;
    private String password;
    private String email;
    private String avatar;
 /**Method to initializes parameters
 * 
 *  
 */    
    public User()
    {
        this.name = "";
        this.firstLastName = "";
        this.secondLastName = "";
        this.nickname = ""; 
        this.password = "";
        this.email =" ";
        this.avatar = "";
    }
 /**Method to initializes parameters
 * @param name 
 * @param firstLastName 
 * @param secondLastName 
 * @param nickname 
 * 
 */   
    public User(String name, String firstLastName, String secondLastName, String nickname) {
        this.name = name;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.nickname = nickname;
    }

/**Method to initializes parameters
 * @param name 
 * @param firstLastName 
 * @param secondLastName
 * @param birthdate 
 * @param nickname 
 * @param password
 * @param email 
 * @param avatar 
 * 
 */   
    

    public User(String name, String firstLastName, String secondLastName, Date birthdate, String nickname, String password, String email, String avatar) {
        this.name = name;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.birthdate = birthdate;
        this.nickname = nickname;
        this.password = password;
        this.email = email;
        this.avatar = avatar;
    }
    
/**Method to get the name of the user
 * 
 * @return name
 */    
    public String getName() {
        return name;
    }
/**Method to set the name of the user
 * 
 * @param name 
 */
    public void setName(String name) {
        this.name = name;
    }
/**Method to get the first last name of the user
 * 
 * @return firtstLastName
 */
    public String getFirstLastName() {
        return firstLastName;
    }
 /** Method to set the first last name of the user
 * @param firstLastName 
 */
    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }
/**Method to get the second last name
 * 
 * @return secondLastName
 */
    public String getSecondLastName() {
        return secondLastName;
    }
/**Method to set the second last name
 * 
 * @param secondLastName 
 */
    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }
/**Method to get the birthdate
 * 
 * @return birthdate
 */
    public Date getBirthdate() {
        return birthdate;
    }
/**Method to set the birthdate
 * 
 * @param birthdate 
 */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
/**Method to get de nickname
 * 
 * @return nickname
 */
    public String getNickname() {
        return nickname;
    }
/**Method to set de nickname
 * 
 * @param nickname 
 */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
/**Method to get the password
 * 
 * @return password
 */
    public String getPassword() {
        return password;
    }
/**Method to get the email
 * 
 * @return email
 */
    public String getEmail() {
        return email;
    }
/**Method to set the email
 * 
 * @param email 
 */

    public void setEmail(String email) {
        this.email = email;
    }
/**Method to get tha avatar
 * 
 * @return avatar
 */
    public String getAvatar() {
        return avatar;
    }
/**Method to set the avatar
 * 
 * @param avatar 
 */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
/**Method to set the password
 * 
 * @param password 
 */
    public void setPassword(String password) {
        this.password = password;
    }

    
    
}
