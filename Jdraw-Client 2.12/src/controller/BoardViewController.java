/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.Color;
import java.util.Iterator;
import javax.swing.JComboBox;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import jwhiteboard.whiteboard.WhiteBoard;
import model.ListFriends;
import model.ListUser;
import model.Preferences;
import model.PreferencesXMLFile;
import model.StandardObject;
import model.User;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.JLabel;
import javax.swing.JTextField;
import model.ListColor;

/**Class to manage the Board VIew
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */
public class BoardViewController 
{
    private static WhiteBoard myBoard;
    private static JComboBox listfriends;
    private static JComboBox listcolors;
    private static JLabel jlnicknameuser;
    private static JTextField jtfword;
    
 /**Method to initiate the internal components of BoardView
  * 
  * @param myWhiteBoard 
  * @param ListFriends 
  */   
    public static void initOutlets(WhiteBoard myWhiteBoard,JComboBox ListFriends,JComboBox ListColors, JLabel NicknameFriend, JTextField Word)
    {
        BoardViewController.listfriends= ListFriends;
        BoardViewController.myBoard = myWhiteBoard;
        BoardViewController.listcolors= ListColors;
        BoardViewController.jlnicknameuser=NicknameFriend;
        BoardViewController.jtfword=Word;
      
        
    }
 /**Method to configure the White Board
  * 
  * @param Large 
  * @param color
  */   
    public static void configureWhiteBoard(int Large, Color color)
    {
        BoardViewController.myBoard.setBackground(Color.white);
        BoardViewController.myBoard.setColor(color);
        BoardViewController.myBoard.setLarge(Large);
    }


/**Method to save the image painted in the white board
 * 
 */    
public static void saveImageFromWhiteBoard()
    {
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        Calendar fecha= Calendar.getInstance();
        String Day=Integer.toString(fecha.get(Calendar.DATE));
        String Month=Integer.toString(fecha.get(Calendar.MONTH));
        String year=Integer.toString(fecha.get(Calendar.YEAR));
        String Hour=Integer.toString(fecha.get(Calendar.HOUR_OF_DAY));
        String Minutes=Integer.toString(fecha.get(Calendar.MINUTE));
        String Seconds=Integer.toString(fecha.get(Calendar.SECOND));
        System.out.println ("Day: "+Day+" Month: "+Month+" Year:"+year+" Hour: "+Hour);

        
        BoardViewController.myBoard.saveWhiteBoardAsJPG(preferences.getFolderpath()+"/"+LoginViewController.jTFNickname.getText()+Day+Month+year+Hour+Minutes+Seconds+".jpg");
    }
    
/**Method to fill the combo users
 * 
 */  
      public static void loadAllUsersInCombo()
    {
        SocketClient clientRequest = new SocketClient();
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        StandardObject objectToServer;
        byte [] answer;   
        
        
       
         objectToServer = new StandardObject("4:"+LoginViewController.jTFNickname.getText(), null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        
        final ListFriends la = (ListFriends) UtilFunctions.byteArrayToObject(answer);
        
        Iterator listIterator = la.iterator();
        
        listfriends.removeAllItems();
        
        while (listIterator.hasNext())
        {
            User actualUser = (User) listIterator.next();
            
            String information = actualUser.getFirstLastName()+ ", " + actualUser.getName()+ ", NickName"+ " : " + actualUser.getNickname();
            
            listfriends.addItem(information);  
            
        }
                       listfriends.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                            String prueba= listfriends.getSelectedItem().toString();
                            String[] nickname=prueba.split(" : ");
                            System.out.println("Username: " + nickname[1]);
                           User usercombo=la.searchfriend(nickname[1]);
                            if (usercombo != null){
                                BoardViewController.jlnicknameuser.setText(usercombo.getNickname());

                             }

			}
		});
    
    }
    
    public static void loadAllColorsInCombo()
    {
        SocketClient clientRequest = new SocketClient();
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        StandardObject objectToServer;
        byte [] answer;   
        
        
       
         objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText(), null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        
        ListColor la = (ListColor) UtilFunctions.byteArrayToObject(answer);
        
        Iterator listIterator = la.iterator();
        
        listcolors.removeAllItems();
        
        while (listIterator.hasNext())
        {
            Color actualColor = (Color) listIterator.next();
            int actualColorCode = actualColor.getRGB();
            String colorCode= Integer.toString(actualColorCode);
            String information = colorCode;
            
            listcolors.addItem(information);  
            
        }
    }
 /**Method to LogOut from Jdraw
 * 
 * @return boolean
 */  
        public static boolean logOutUser()
    { 
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer;
        
        objectToServer = new StandardObject("16:"+LoginViewController.jTFNickname.getText(), null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Logged");
           return true;}
        else
            System.out.println("Error User not in JdrawDatabase");
            System.out.println("Se recibio del servidor: " + UtilFunctions.byteArrayToString(answer));
            return false;
        
  }
        
        public static void loadAllColors2InCombo()
    {
        SocketClient clientRequest = new SocketClient();
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        StandardObject objectToServer;
        byte [] answer;   
        
        
       
        objectToServer = new StandardObject("22:"+LoginViewController.jTFNickname.getText(), null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        
        ListColor la = (ListColor) UtilFunctions.byteArrayToObject(answer);
        
        Iterator listIterator = la.iterator();
        
        listcolors.removeAllItems();
        
        while (listIterator.hasNext())
        {
            Color actualColor = (Color) listIterator.next();
            
            
            
            //listcolors.addItem(information);  
            
        }
    }
        
    public static void loadColorPalette()
    {
        
        byte [] answer;
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();
        StandardObject objectToServer;
        objectToServer = new StandardObject("23:"+LoginViewController.jTFNickname.getText(),null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
        String[] palette= UtilFunctions.byteArrayToString(answer).split(",");
        int a=0;
        while (a<=3)
        {
        if (palette[a].compareTo("PastelsPack")== 0)
        {
            System.out.println("entre");
            //First color 
            Color cream = new Color (0x00,0xFF,0x99);
            int creamCode = cream.getRGB();
            String creamColorCode = Integer.toString(creamCode);
            Color colorCream = Color.decode(creamColorCode);
            //Second Color
            Color creamPink = new Color (0xFF,0xCC,0xCC);
            int creamPinkCode = creamPink.getRGB();
            String creamPinkColorCode = Integer.toString(creamPinkCode);
            Color colorCreamPink = Color.decode(creamPinkColorCode);
            //Third Color
            Color creamCyan = new Color (0xCC,0xFF,0xCC);
            int creamCyanCode = creamCyan.getRGB();
            String creamCyanColorCode = Integer.toString(creamCyanCode);
            Color colorCreamCyan = Color.decode(creamCyanColorCode);
            //Fourth Color
            Color creamGreen = new Color (0xCC,0xFF,0x99);
            int creamGreenCode = creamGreen.getRGB();
            String creamGreenColorCode = Integer.toString(creamGreenCode);
            Color colorCreamGreen = Color.decode(creamGreenColorCode);
            //save colors in the list
            ListColor colorList = new ListColor();
            colorList.addColor(colorCream);
            colorList.addColor(colorCreamPink);
            colorList.addColor(colorCreamCyan);
            colorList.addColor(colorCreamGreen);
            Iterator listIterator = colorList.iterator();
            while (listIterator.hasNext())
            {
                Color actualColor = (Color) listIterator.next();
                int actualColorCode = actualColor.getRGB();
                String colorCode= Integer.toString(actualColorCode);
                if (colorCode.compareTo("-16711783")==0)
                {
                    String information = "Cream";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-13108")==0)
                {
                    String information = "Cream Pink";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-3342388")==0)
                {
                    String information = "Cream Cyan";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-3342439")==0)
                {
                    String information = "Cream Green";
                    listcolors.addItem(information);
                }
            }
            
        }
        if (palette[a].compareTo("FluorecentPack")== 0)
        {
            //First color 
            Color cyan = new Color (0x00,0xFF,0xFF);
            int cyanCode = cyan.getRGB();
            String cyanColorCode = Integer.toString(cyanCode);
            Color colorCyan = Color.decode(cyanColorCode);
            //Second Color
            Color fluorecentGreen = new Color (0x00,0xFF,0x33);
            int fluorecentGreenCode = fluorecentGreen.getRGB();
            String fluorecentGreenColorCode = Integer.toString(fluorecentGreenCode);
            Color colorFluorecentGreen = Color.decode(fluorecentGreenColorCode);
            //Third Color
            Color fluorecentYellow = new Color (0xCC,0xFF,0x33);
            int fluorecentYellowCode = fluorecentYellow.getRGB();
            String fluorecentYellowColorCode = Integer.toString(fluorecentYellowCode);
            Color colorFluorecentYellow = Color.decode(fluorecentYellowColorCode);
            //Fourth Color
            Color fluorecentRed = new Color (0xFF,0x00,0x66);
            int fluorecentRedCode = fluorecentRed.getRGB();
            String fluorecentRedColorCode = Integer.toString(fluorecentRedCode);
            Color colorFluorecentRed = Color.decode(fluorecentRedColorCode);
            //save colors in the list
            ListColor colorList = new ListColor();
            colorList.addColor(colorCyan);
            colorList.addColor(colorFluorecentGreen);
            colorList.addColor(colorFluorecentYellow);
            colorList.addColor(colorFluorecentRed);
            Iterator listIterator = colorList.iterator();
            while (listIterator.hasNext())
            {
                Color actualColor = (Color) listIterator.next();
                int actualColorCode = actualColor.getRGB();
                String colorCode= Integer.toString(actualColorCode);
                if (colorCode.compareTo("-16711681")==0)
                {
                    String information = "Cyan";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-16711885")==0)
                {
                    String information = "Fluorecent Green";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-3342541")==0)
                {
                    String information = "Fluorecent Yellow";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-65434")==0)
                {
                    String information = "Fluorecent Red";
                    listcolors.addItem(information);
                }
            }
        }
        if (palette[a].compareTo("BasicPack")== 0)
        {
            //First color 
            Color red = new Color (0xFF,0x00,0x00);
            int redCode = red.getRGB();
            String redColorCode = Integer.toString(redCode);
            Color colorRed = Color.decode(redColorCode);
            //Second Color
            Color green = new Color (0x66,0x99,0x00);
            int greenCode = green.getRGB();
            String greenColorCode = Integer.toString(greenCode);
            Color colorGreen = Color.decode(greenColorCode);
            //Third Color
            Color yellow = new Color (0xFF,0xFF,0x00);
            int yellowCode = yellow.getRGB();
            String yellowColorCode = Integer.toString(yellowCode);
            Color colorYellow = Color.decode(yellowColorCode);
            //Fourth Color
            Color blue = new Color (0x66,0x00,0xFF);
            int blueCode = blue.getRGB();
            String blueColorCode = Integer.toString(blueCode);
            Color colorBlue = Color.decode(blueColorCode);
            //save colors in the list
            ListColor colorList = new ListColor();
            colorList.addColor(colorRed);
            colorList.addColor(colorGreen);
            colorList.addColor(colorYellow);
            colorList.addColor(colorBlue);
            Iterator listIterator = colorList.iterator();
            while (listIterator.hasNext())
            {
                Color actualColor = (Color) listIterator.next();
                int actualColorCode = actualColor.getRGB();
                String colorCode= Integer.toString(actualColorCode);
                if (colorCode.compareTo("-65536")==0)
                {
                    String information = "Red";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-10053376")==0)
                {
                    String information = "Green";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-256")==0)
                {
                    String information = "Yellow";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-10092289")==0)
                {
                    String information = "Blue";
                    listcolors.addItem(information);
                }
            }
        }
        if (palette[a].compareTo("VintangePack")== 0)
        {
            //First color 
            Color wine = new Color (0x99,0x00,0x33);
            int wineCode = wine.getRGB();
            String wineColorCode = Integer.toString(wineCode);
            Color colorWine = Color.decode(wineColorCode);
            //Second Color
            Color lightPurple = new Color (0xCC,0x66,0x99);
            int lightPurpleCode = lightPurple.getRGB();
            String lightPurpleColorCode = Integer.toString(lightPurpleCode);
            Color colorLightPurple = Color.decode(lightPurpleColorCode);
            //Third Color
            Color mustard = new Color (0xCC,0x99,0x33);
            int mustardCode = mustard.getRGB();
            String mustardColorCode = Integer.toString(mustardCode);
            Color colorMustard = Color.decode(mustardColorCode);
            //Fourth Color
            Color aqua = new Color (0x66,0xFF,0x99);
            int aquaCode = aqua.getRGB();
            String aquaColorCode = Integer.toString(aquaCode);
            Color colorAqua = Color.decode(aquaColorCode);
            //save colors in the list
            ListColor colorList = new ListColor();
            colorList.addColor(colorWine);
            colorList.addColor(colorLightPurple);
            colorList.addColor(colorMustard);
            colorList.addColor(colorAqua);
            Iterator listIterator = colorList.iterator();
            while (listIterator.hasNext())
            {
                Color actualColor = (Color) listIterator.next();
                int actualColorCode = actualColor.getRGB();
                String colorCode= Integer.toString(actualColorCode);
                if (colorCode.compareTo("-6750157")==0)
                {
                    String information = "Wine";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-3381607")==0)
                {
                    String information = "Light Purple";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-3368653")==0)
                {
                    String information = "Mustard";
                    listcolors.addItem(information);
                }
                if (colorCode.compareTo("-10027111")==0)
                {
                    String information = "Aqua";
                    listcolors.addItem(information);
                }
            }
        }
        a++;
        }
    }
    
    public static Color setColorBrush(Object color)
    {
        String selectedColor = color.toString();
        Color brushColor=null;
        if(selectedColor.compareTo("Cream")==0)
        {
            String colorCode = "-16711783"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Cream Pink")==0)
        {
            String colorCode = "-13108"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Cream Cyan")==0)
        {
            String colorCode = "-3342388"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Cream Green")==0)
        {
            String colorCode = "-3342439"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Cyan")==0)
        {
            String colorCode = "-16711681"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Fluorecent Green")==0)
        {
            String colorCode = "-16711885"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Fluorecent Yellow")==0)
        {
            String colorCode = "-3342541"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Fluorecent Red")==0)
        {
            String colorCode = "-65434"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Red")==0)
        {
            String colorCode = "-65536"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Green")==0)
        {
            String colorCode = "-10053376"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Yellow")==0)
        {
            String colorCode = "-256"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Blue")==0)
        {
            String colorCode = "-10092289"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Wine")==0)
        {
            String colorCode = "-6750157"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Light Purple")==0)
        {
            String colorCode = "-3381607"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Mustard")==0)
        {
            String colorCode = "-3368653"; 
            brushColor = Color.decode(colorCode);
        }
        if(selectedColor.compareTo("Aqua")==0)
        {
            String colorCode = "-10027111"; 
            brushColor = Color.decode(colorCode);
        }
        return brushColor;
    }
    
 /**Method to Set Template on Board
 * @param Path
 * 
 */  
    public static void setTemplate(String Path) {
        BoardViewController.myBoard.setBackGroundImage(Path, true);
       
    }
    public static boolean playjDraw()
{
        BoardViewController.myBoard.saveWhiteBoardAsJPG("src/PlayImages/"+LoginViewController.jTFNickname.getText()+BoardViewController.jlnicknameuser.getText()+BoardViewController.jtfword.getText()+".jpg");
       
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer;
        
        objectToServer = new StandardObject("11:"+LoginViewController.jTFNickname.getText()+":"+BoardViewController.jlnicknameuser.getText()+":"+BoardViewController.jtfword.getText(), null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Logged");
           return true;}
        else
            System.out.println("Error User not in JdrawDatabase");
            System.out.println("Se recibio del servidor: " + UtilFunctions.byteArrayToString(answer));
            return false;
}
        public static boolean saveimagemathinserver(){
          String path;
        path = "src/PlayImages/"+LoginViewController.jTFNickname.getText()+BoardViewController.jlnicknameuser.getText()+BoardViewController.jtfword.getText()+".jpg";
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();
        
        StandardObject objectToServer;
        byte [] answer,image;
        image=UtilFunctions.fileToByteArray(path);
        
        objectToServer = new StandardObject("20:"+LoginViewController.jTFNickname.getText()+":"+BoardViewController.jlnicknameuser.getText()+":"+BoardViewController.jtfword.getText(), image);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Logged");
           return true;}
        else
            System.out.println("Error User not in JdrawDatabase");
            System.out.println("Se recibio del servidor: " + UtilFunctions.byteArrayToString(answer));
            return false;
    }


}
