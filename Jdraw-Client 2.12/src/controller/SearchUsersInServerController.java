/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import jsockets.util.UtilFunctions;
import model.ListUser;
import model.Preferences;
import model.PreferencesXMLFile;
import model.StandardObject;
import model.User;

/**Class to manage the Search Users in server
 *
  * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */
public class SearchUsersInServerController {
       private static JComboBox listUser;
       private static JLabel jLNameField,jLFirstLastNameField,jLSecondLastNameField,jLNickNameField,jLAvatarfield;
/**Method to initiate the internal components of SearchUsersInServerController    
 * 
 * @param listUser
 * @param jLNameField
 * @param jLFirstLastNameField
 * @param jLSecondLastNameField
 * @param jLNickNameField
 * @param jLAvatar 
 */    
    
    public static void initOutlets(JComboBox listUser,JLabel jLNameField,JLabel jLFirstLastNameField,JLabel jLSecondLastNameField,JLabel jLNickNameField,JLabel jLAvatar)
    {
        SearchUsersInServerController.listUser= listUser;
        SearchUsersInServerController.jLNameField=jLNameField;
        SearchUsersInServerController.jLFirstLastNameField=jLFirstLastNameField;
        SearchUsersInServerController.jLSecondLastNameField=jLSecondLastNameField;
        SearchUsersInServerController.jLNickNameField=jLNickNameField;
        SearchUsersInServerController.jLAvatarfield=jLAvatar;
    }
   /**Method to load all users in server
    * 
    */
    
    public static void loadAllUsersInCombo()
    {
        SocketClient clientRequest = new SocketClient();
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        StandardObject objectToServer;
        byte [] answer;   
        
        
       
         objectToServer = new StandardObject("3:", null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        
        final ListUser la = (ListUser) UtilFunctions.byteArrayToObject(answer);
       
        
        Iterator listIterator = la.iterator();
        
        listUser.removeAllItems();
        
        while (listIterator.hasNext())
        {
            User actualUser = (User) listIterator.next();
           if (LoginViewController.jTFNickname.getText().equals(actualUser.getNickname())==false){
             String information = actualUser.getFirstLastName()+ ", " + actualUser.getName()+ ", NickName"+ " : " + actualUser.getNickname();
            
            listUser.addItem(information);  }
            
        }
      listUser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                            String prueba= listUser.getSelectedItem().toString();
                            String[] nickname=prueba.split(" : ");
                            System.out.println("Username: " + nickname[1]);
                            User usercombo=la.searchUser(nickname[1]);
                            if (usercombo != null){
                                SearchUsersInServerController.jLNameField.setText(usercombo.getName());
                                SearchUsersInServerController.jLFirstLastNameField.setText(usercombo.getFirstLastName());
                                SearchUsersInServerController.jLNickNameField.setText(usercombo.getNickname());
                                SearchUsersInServerController.jLSecondLastNameField.setText(usercombo.getSecondLastName());
                             }
		         
			}
		});
       
    
    }
    /**Method to load the AvatarUser from server
    * 
    */
        public static void AvatarFriend(){
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();
        StandardObject objectToServer;
        byte [] answer;
         System.out.println("Username: " + jLNickNameField.getText());
        objectToServer = new StandardObject("19:"+jLNickNameField.getText(), null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        
        try
        {
            
            UtilFunctions.createFileFromByteArray(answer, "src/friendsavatar/",jLNickNameField.getText(), "jpg", true);
        } 
        catch (Exception e)
        {
  
        }
        ImageIcon image=new ImageIcon("src/friendsavatar/"+jLNickNameField.getText()+".jpg");
        if ((image.getIconHeight()>342) || (image.getIconWidth()>230))
         {
             ImageIcon imageScalada=new ImageIcon(image.getImage().getScaledInstance(108, 103, 600));
            
             SearchUsersInServerController.jLAvatarfield.setIcon(imageScalada);
         }
         else
             SearchUsersInServerController.jLAvatarfield.setIcon(image);
    }
   /**Method to send invitation to users
    * 
    * @return boolean
    */
   public static boolean RequestInvitation(){
       Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
       SocketClient clientRequest = new SocketClient();
       StandardObject objectToServer;
       byte [] answer;
       objectToServer = new StandardObject("6:"+LoginViewController.jTFNickname.getText()+":"+SearchUsersInServerController.jLNickNameField.getText(), null);
       answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
       if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Invitacion enviada con exito");
        return true;}
        else
            System.out.println("Error registrando al usuario");
        return false;    
   }
}

   
  
