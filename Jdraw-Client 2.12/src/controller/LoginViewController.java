/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import javax.swing.JLabel;
import javax.swing.JTextField;
import model.Preferences;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import model.PreferencesXMLFile;
import model.StandardObject;

/**Class to manage the login of the users
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */
public class LoginViewController 
{ 
    public static JTextField jTFNickname;
 /**Method to initiate the internal components of LoginViewView
  * 
  * @param JTFNickname 
  */
    public static void initOutlets (JTextField JTFNickname){
        LoginViewController.jTFNickname=JTFNickname;
    }
 /**Method to set the preferences of the app client
  * 
  * @param userip
  * @param userport
  * @param userfolder
  * @return boolean if set preferences was successuful
  */
    public static boolean setpreferences(String userip,int userport,String userfolder){
        Preferences preferences=new Preferences(userip,userport,userfolder);
       if (PreferencesXMLFile.savePreferencesInJdrawDataBase(preferences)==true)
           return true;
                   else return false;
    }
 /**Method to get the preferences of the app cliente
  * 
  * @param userip
  * @param userport
  * @param userfolder
  * @return preferences of app client
  */
     public static Preferences getpreferences(String userip,int userport,String userfolder){
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        return preferences;
     }
   /**Method to log in 
    * 
    * @param nickname
    * @param password
    * @return boolean
    */       
    public static boolean loginUser(String nickname,String password)
    { 
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer;
        
        objectToServer = new StandardObject("1:"+nickname+":"+password, null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Logged");
           return true;}
        else
            System.out.println("Error User not in JdrawDatabase");
            System.out.println("Se recibio del servidor: " + UtilFunctions.byteArrayToString(answer));
            return false;
        
  }
}
