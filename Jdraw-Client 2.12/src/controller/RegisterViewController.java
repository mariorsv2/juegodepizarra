/*
 * This its the controller class for the register window
 */
package controller;

import com.toedter.calendar.JDateChooser;
import java.text.ParseException;
import java.util.Date;
import model.StandardObject;
import model.User;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import model.Preferences;
import model.PreferencesXMLFile;


/**Class to manage the Register View
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */

public class RegisterViewController 
{


/**Method to set preferences of the app client
 * 
 * @param userip
 * @param userport
 * @param userfolder
 * @return booleam 
 */    
   public static boolean setpreferences(String userip,int userport,String userfolder)
    {
        Preferences preferences=new Preferences(userip,userport,userfolder);
       if (PreferencesXMLFile.savePreferencesInJdrawDataBase(preferences)==true)
           return true;
                   else return false;
    }
/**Method to get the preferences of the app cliente
 * 
 * @param userip
 * @param userport
 * @param userfolder
 * @return preferences
 */     
   public static Preferences getpreferences(String userip,int userport,String userfolder)
     {
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        return preferences;
     }
 /**Method to send the user to the server to registed
  * 
  * @param name
  * @param firstlastname
  * @param secondlastname
  * @param birthdate
  * @param nickname
  * @param password
  * @param email
  * @param avatar
  * @return boolean
  */  
    public static boolean sendUserToServer (String name,String firstlastname,String secondlastname,Date birthdate,String nickname, String password, String email, String avatar)
    {
        
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer;
        
        User newUser = new User(name,firstlastname,secondlastname,birthdate,nickname,password,email,avatar);
        objectToServer = new StandardObject("2:", newUser);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
      
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Usuario registrado con exito");
           return true;}
        else
            System.out.println("Nickname ya en uso");
        
            return false;
            
            
    }
      
    
}


