/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import model.Preferences;
import model.PreferencesXMLFile;
import model.StandardObject;
import model.User;
import view.EditUserView;

/**Class to manage the EditView
 *
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */
public class EditUserViewController 
{
    public static JTextField  jTFEmail,jTFFirstLastName,jTFName,jTFNickname,jTFPathEdit,jTFSecondLastName;
    public static JDateChooser jDCBirth;
    public static JPasswordField jPFConfirme, jPFFirst;
    public static JLabel jLAvatar;
    /**Method to initiate the internal components of BoardView
  * 
  * @param jDCBirtj
  * @param jTFEmail 
  * @param jTFFirstLastName 
  * @param jTFName 
  * @param jTFSecondLastName 
  * @param jTFNickname 
  * @param jTFPathEdit 
  * @param jPFFirst 
  * @param jPFConfirme 
  * @param jLavatar 
  * @param ListFriends 
  */  
    public static void initOutlets(JDateChooser jDCBirth, JTextField jTFEmail, JTextField jTFFirstLastName ,JTextField jTFName, JTextField jTFNickname,JTextField jTFPathEdit,JTextField jTFSecondLastName, JPasswordField jPFConfirme, JPasswordField jPFFirst, JLabel jLavatar)
    {
        EditUserViewController.jTFEmail=jTFEmail;
        EditUserViewController.jTFFirstLastName=jTFFirstLastName;
        EditUserViewController.jTFName=jTFName;
        EditUserViewController.jTFNickname=jTFNickname;
        EditUserViewController.jTFPathEdit=jTFPathEdit;
        EditUserViewController.jTFSecondLastName=jTFSecondLastName;    
        EditUserViewController.jDCBirth=jDCBirth;
        EditUserViewController.jPFFirst=jPFFirst;
        EditUserViewController.jPFConfirme=jPFConfirme;
        EditUserViewController.jLAvatar= jLavatar;
        
    }
  /**Method to Search User 
  * 
  * @param Search 
  * @return boolean
  */  
    public static boolean UserSearch(String Search)
    {
       Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
       SocketClient clientRequest = new SocketClient();
       StandardObject objectToServer;
       byte [] answer;
       objectToServer = new StandardObject("5:"+LoginViewController.jTFNickname.getText(), null);
       answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
//       StandardObject objectToServer2;
//       String[] protocolo = objectToServer2.getProtocol().toString().split(":");
       User userfriend= (User)UtilFunctions.byteArrayToObject(answer);
            
          if (userfriend!=null)
         {
           System.out.println("Found");
           System.out.println("Name: " + userfriend.getName());//
           System.out.println("First lastname: " + userfriend.getFirstLastName());
           System.out.println("Second lastname: " + userfriend.getSecondLastName());
           
           EditUserViewController.jTFEmail.setText(userfriend.getEmail());
           EditUserViewController.jTFFirstLastName.setText(userfriend.getFirstLastName());
           EditUserViewController.jTFSecondLastName.setText(userfriend.getSecondLastName());
           EditUserViewController.jTFName.setText(userfriend.getName());
           EditUserViewController.jTFNickname.setText(userfriend.getNickname());
           EditUserViewController.jPFFirst.setText(userfriend.getPassword());
           EditUserViewController.jPFConfirme.setText(userfriend.getPassword());
           EditUserViewController.jTFPathEdit.setText(userfriend.getAvatar());
           EditUserViewController.jDCBirth.setDate(userfriend.getBirthdate());
           return true;
         }
        else
            System.out.println("Error User not in JdrawDatabase");
            System.out.println("Se recibio del servidor: " + UtilFunctions.byteArrayToString(answer));
            return false;
    }
    
  /**Method to Obtain Avatar For User
  * 
  * @param Search 
  * @return boolean
  */  
       public static void AvatarUser(String search){
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();
        StandardObject objectToServer;
        byte [] answer;
        objectToServer = new StandardObject("19:"+search, null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        
        try
        {
            
            UtilFunctions.createFileFromByteArray(answer, "src/friendsavatar/",search, "jpg", true);
        } 
        catch (Exception e)
        {
  
        }
        ImageIcon image=new ImageIcon("src/friendsavatar/"+search+".jpg");
        if ((image.getIconHeight()>342) || (image.getIconWidth()>230))
         {
             ImageIcon imageScalada=new ImageIcon(image.getImage().getScaledInstance(108, 103, 600));
            
             EditUserViewController.jLAvatar.setIcon(imageScalada);
         }
         else
             EditUserViewController.jLAvatar.setIcon(image);
    }
    
    
  /**Method to Send the information the user
  * 
  * @param name 
  * @param firstlastname 
  * @param secondlastname 
  * @param birthdate 
  * @param nickname 
  * @param password 
  * @param email 
  * @param avatar 
  * @return boolean
  */  
    
    public static boolean sendUpdateUserToServer (String name,String firstlastname,String secondlastname,Date birthdate,String nickname, String password, String email, String avatar)
    {
  
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer;
        
        User newUser = new User(name,firstlastname,secondlastname,birthdate,nickname,password,email,avatar);
        objectToServer = new StandardObject("9:", newUser);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
      
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Usuario actualizado con exito");
           return true;}
        else
            System.out.println("Nickname ya en uso");
        
            return false;
    }
       public static boolean sendAvatarToServer ( String avatar)
    {
  
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer,image;
        image=UtilFunctions.fileToByteArray(avatar);
        objectToServer = new StandardObject("14:"+LoginViewController.jTFNickname.getText(), image);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
      
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Usuario actualizado con exito");
           return true;}
        else
            System.out.println("Nickname ya en uso");
        
            return false;
    }
    
    
}
