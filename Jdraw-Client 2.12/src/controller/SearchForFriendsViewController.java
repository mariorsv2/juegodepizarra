/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JTextField;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import model.Preferences;
import model.PreferencesXMLFile;
import model.StandardObject;
import model.User;
import view.SearchForFriendsView;

/**Class to manage the Search Friends View Controller
 *
  * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */
public class SearchForFriendsViewController 
{
    public static JLabel jLNameField,jLFirstLastNameField,jLSecondLastNameField,jLAvatar;
    public static JTextField jTFnickname;
 /**Method to initiate the internal components of SearchForFriendsViewController 
  * 
  * @param jLNameFieldV
  * @param jLFirstLastNameFieldV
  * @param jLSecondLastNameFieldV
  * @param jLAvatar
  * @param jTFNickName 
  */ 
     public static void initOutlets(JLabel jLNameFieldV,JLabel jLFirstLastNameFieldV,JLabel jLSecondLastNameFieldV,JLabel jLAvatar,JTextField jTFNickName)
    {
        SearchForFriendsViewController.jLNameField=jLNameFieldV;
        SearchForFriendsViewController.jLFirstLastNameField=jLFirstLastNameFieldV;
        SearchForFriendsViewController.jLSecondLastNameField=jLSecondLastNameFieldV;
        SearchForFriendsViewController.jLAvatar=jLAvatar;
        SearchForFriendsViewController.jTFnickname=jTFNickName;
    }
 /**Method to send the avatar of the user to the server
  * 
  * @param search 
  */
    
    public static void AvatarFriend(String search){
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();
        StandardObject objectToServer;
        byte [] answer;
        objectToServer = new StandardObject("19:"+search, null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        
        try
        {
            
            UtilFunctions.createFileFromByteArray(answer, "src/friendsavatar/",search, "jpg", true);
        } 
        catch (Exception e)
        {
  
        }
        ImageIcon image=new ImageIcon("src/friendsavatar/"+search+".jpg");
        if ((image.getIconHeight()>342) || (image.getIconWidth()>230))
         {
             ImageIcon imageScalada=new ImageIcon(image.getImage().getScaledInstance(108, 103, 600));
            
             SearchForFriendsViewController.jLAvatar.setIcon(imageScalada);
         }
         else
             SearchForFriendsViewController.jLAvatar.setIcon(image);
    }
  /**Method to look for a user in the server
   * 
   * @param Search
   * @return boolean
   */
   public static boolean NicknameSearch(String Search)
    {
       Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
       SocketClient clientRequest = new SocketClient();
       StandardObject objectToServer;
       byte [] answer;
       objectToServer = new StandardObject("5:"+Search, null);
       answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);

       User userfriend= (User)UtilFunctions.byteArrayToObject(answer);
            
          if (userfriend!=null)
         {
           System.out.println("Found");
           System.out.println("Name: " + userfriend.getName());//
           System.out.println("First lastname: " + userfriend.getFirstLastName());
           System.out.println("Second lastname: " + userfriend.getSecondLastName());
           
           SearchForFriendsViewController.jLNameField.setText(userfriend.getName());
           SearchForFriendsViewController.jLFirstLastNameField.setText(userfriend.getFirstLastName());
           SearchForFriendsViewController.jLSecondLastNameField.setText(userfriend.getSecondLastName());
           return true;
         }
        else
            System.out.println("Error User not in JdrawDatabase");
            System.out.println("Se recibio del servidor: " + UtilFunctions.byteArrayToString(answer));
            return false;
    }
 /**Method to send the request to the other user
  * 
  * @return boolean
  */
   public static boolean RequestInvitation(){
       Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
       SocketClient clientRequest = new SocketClient();
       StandardObject objectToServer;
       byte [] answer;
       objectToServer = new StandardObject("6:"+LoginViewController.jTFNickname.getText()+":"+SearchForFriendsViewController.jTFnickname.getText(), null);
       answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
       if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Invitacion enviada con exito");
        return true;}
        else
            System.out.println("Error registrando al usuario");
        return false;    
   }
}

    
