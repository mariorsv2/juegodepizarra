/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.Color;
import javax.swing.JButton;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import model.Preferences;
import model.PreferencesXMLFile;
import model.StandardObject;
import model.Store;
import model.User;
import model.ListColor;

/**
 *
 * @author Ryu Alejo
 */
public class StoreViewController 
{
    private static JButton jBColorPack;
    
    public static void initOutlets(JButton ColorPacks)
    {
        StoreViewController.jBColorPack=ColorPacks;
    }
    
    public static boolean buyBombs(int bombs, int price)
    {
        String bomb=Integer.toString(bombs);
        //String newBombs = Integer.toString(bomb.getBombs());
        
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();
        StandardObject objectToServer;
        byte [] answer;
        objectToServer = new StandardObject("15:"+LoginViewController.jTFNickname.getText()+":"+bomb+":"+price,null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0){
            System.out.println("Purchase Succesfull");
           return true;}
        else
            System.out.println("Purchase Failed");
        
            return false;
    }
    
//    public static boolean buyColorPalette (int price, String paletteType)
//    {
//        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
//        SocketClient clientRequest = new SocketClient();
//        StandardObject objectToServer;
//        byte [] answer;
//        String selectedPalette = paletteType;
//        if (selectedPalette.compareTo("PastelsPack")==0)
//        {
//            //First color 
//            Color cream = new Color (0x00,0xFF,0x99);
//            int creamCode = cream.getRGB();
//            String creamColorCode = Integer.toString(creamCode);
//            Color colorCream = Color.decode(creamColorCode);
//            //Second Color
//            Color creamPink = new Color (0xFF,0xCC,0xCC);
//            int creamPinkCode = creamPink.getRGB();
//            String creamPinkColorCode = Integer.toString(creamPinkCode);
//            Color colorCreamPink = Color.decode(creamPinkColorCode);
//            //Third Color
//            Color creamCyan = new Color (0xCC,0xFF,0xCC);
//            int creamCyanCode = creamCyan.getRGB();
//            String creamCyanColorCode = Integer.toString(creamCyanCode);
//            Color colorCreamCyan = Color.decode(creamCyanColorCode);
//            //Fourth Color
//            Color creamGreen = new Color (0xCC,0xFF,0x99);
//            int creamGreenCode = creamGreen.getRGB();
//            String creamGreenColorCode = Integer.toString(creamGreenCode);
//            Color colorCreamGreen = Color.decode(creamGreenColorCode);
//            //save colors in the list
//            ListColor colorList = new ListColor();
//            colorList.addColor(colorCream);
//            colorList.addColor(colorCreamPink);
//            colorList.addColor(colorCreamCyan);
//            colorList.addColor(colorCreamGreen);
//            objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,colorList);
//            answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
//            if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
//            {
//                System.out.println("Purchase Succesfull");
//                return true;
//            }
//            else
//                System.out.println("Purchase Failed");
//        }
//        
//        
//            if (selectedPalette.compareTo("FluorecentPack")==0)
//            {
//                //First color 
//                Color cyan = new Color (0x00,0xFF,0xFF);
//                int cyanCode = cyan.getRGB();
//                String cyanColorCode = Integer.toString(cyanCode);
//                Color colorCyan = Color.decode(cyanColorCode);
//                //Second Color
//                Color fluorecentGreen = new Color (0x00,0xFF,0x33);
//                int fluorecentGreenCode = fluorecentGreen.getRGB();
//                String fluorecentGreenColorCode = Integer.toString(fluorecentGreenCode);
//                Color colorFluorecentGreen = Color.decode(fluorecentGreenColorCode);
//                //Third Color
//                Color fluorecentYellow = new Color (0xCC,0xFF,0x33);
//                int fluorecentYellowCode = fluorecentYellow.getRGB();
//                String fluorecentYellowColorCode = Integer.toString(fluorecentYellowCode);
//                Color colorFluorecentYellow = Color.decode(fluorecentYellowColorCode);
//                //Fourth Color
//                Color fluorecentRed = new Color (0xFF,0x00,0x66);
//                int fluorecentRedCode = fluorecentRed.getRGB();
//                String fluorecentRedColorCode = Integer.toString(fluorecentRedCode);
//                Color colorFluorecentRed = Color.decode(fluorecentRedColorCode);
//                //save colors in the list
//                ListColor colorList = new ListColor();
//                colorList.addColor(colorCyan);
//                colorList.addColor(colorFluorecentGreen);
//                colorList.addColor(colorFluorecentYellow);
//                colorList.addColor(colorFluorecentRed);
//                objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,colorList);
//                answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
//                if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
//                {
//                    System.out.println("Purchase Succesfull");
//                    return true;
//                }
//                else
//                    System.out.println("Purchase Failed");
//            }
//            
//                if (selectedPalette.compareTo("BasicPack")==0)
//                {
//                    //First color 
//                    Color red = new Color (0xFF,0x00,0x00);
//                    int redCode = red.getRGB();
//                    String redColorCode = Integer.toString(redCode);
//                    Color colorRed = Color.decode(redColorCode);
//                    //Second Color
//                    Color green = new Color (0x66,0x99,0x00);
//                    int greenCode = green.getRGB();
//                    String greenColorCode = Integer.toString(greenCode);
//                    Color colorGreen = Color.decode(greenColorCode);
//                    //Third Color
//                    Color yellow = new Color (0xFF,0xFF,0x00);
//                    int yellowCode = yellow.getRGB();
//                    String yellowColorCode = Integer.toString(yellowCode);
//                    Color colorYellow = Color.decode(yellowColorCode);
//                    //Fourth Color
//                    Color blue = new Color (0x66,0x00,0xFF);
//                    int blueCode = blue.getRGB();
//                    String blueColorCode = Integer.toString(blueCode);
//                    Color colorBlue = Color.decode(blueColorCode);
//                    //save colors in the list
//                    ListColor colorList = new ListColor();
//                    colorList.addColor(colorRed);
//                    colorList.addColor(colorGreen);
//                    colorList.addColor(colorYellow);
//                    colorList.addColor(colorBlue);
//                    objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,colorList);
//                    answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
//                    if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
//                    {
//                        System.out.println("Purchase Succesfull");
//                        return true;
//                    }
//                    else
//                        System.out.println("Purchase Failed");
//                }
//                
//                   if (selectedPalette.compareTo("VintangePack")==0)
//                    {
//                        //First color 
//                        Color wine = new Color (0x99,0x00,0x33);
//                        int wineCode = wine.getRGB();
//                        String wineColorCode = Integer.toString(wineCode);
//                        Color colorWine = Color.decode(wineColorCode);
//                        //Second Color
//                        Color lightPurple = new Color (0xCC,0x66,0x99);
//                        int lightPurpleCode = lightPurple.getRGB();
//                        String lightPurpleColorCode = Integer.toString(lightPurpleCode);
//                        Color colorLightPurple = Color.decode(lightPurpleColorCode);
//                        //Third Color
//                        Color mustard = new Color (0xCC,0x99,0x33);
//                        int mustardCode = mustard.getRGB();
//                        String mustardColorCode = Integer.toString(mustardCode);
//                        Color colorMustard = Color.decode(mustardColorCode);
//                        //Fourth Color
//                        Color aqua = new Color (0x66,0xFF,0x99);
//                        int aquaCode = aqua.getRGB();
//                        String aquaColorCode = Integer.toString(aquaCode);
//                        Color colorAqua = Color.decode(aquaColorCode);
//                        //save colors in the list
//                        ListColor colorList = new ListColor();
//                        colorList.addColor(colorWine);
//                        colorList.addColor(colorLightPurple);
//                        colorList.addColor(colorMustard);
//                        colorList.addColor(colorAqua);
//                        objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,colorList);
//                        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
//                        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
//                        {
//                            System.out.println("Purchase Succesfull");
//                            return true;
//                        }
//                        else
//                            System.out.println("Purchase Failed");
//                    } 
//                   return false;
//    }
    public static boolean buyColorPalette (int price, String paletteType)
    {
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();
        StandardObject objectToServer;
        byte [] answer;
        String selectedPalette = paletteType;
        if (selectedPalette.compareTo("PastelsPack")==0)
        {
            objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,null);
            answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
            if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
            {
                System.out.println("Purchase Succesfull");
                return true;
            }
            else
                System.out.println("Purchase Failed");
        }
        else
        {
            if (selectedPalette.compareTo("FluorecentPack")==0)
            {
                objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,null);
                answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
                if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
                {
                    System.out.println("Purchase Succesfull");
                    return true;
                }
                else
                    System.out.println("Purchase Failed");
            }
            else
            {
                if (selectedPalette.compareTo("BasicPack")==0)
                {
                    objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,null);
                    answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
                    if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
                    {
                        System.out.println("Purchase Succesfull");
                        return true;
                    }
                    else
                        System.out.println("Purchase Failed");
                }
                else
                {
                   if (selectedPalette.compareTo("VintangePack")==0)
                    {
                        objectToServer = new StandardObject("21:"+LoginViewController.jTFNickname.getText()+":"+price+":"+selectedPalette,null);
                        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(),7687);
                        if (UtilFunctions.byteArrayToString(answer).compareTo("TRUE") == 0)
                        {
                            System.out.println("Purchase Succesfull");
                            return true;
                        }
                        else
                            System.out.println("Purchase Failed");
                    } 
                }
            }
        }
        return false;
    }
}
    

