/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import jsockets.client.SocketClient;
import jsockets.util.UtilFunctions;
import model.ListRequest;
import model.ListUser;
import model.Preferences;
import model.PreferencesXMLFile;
import model.StandardObject;
import model.User;

/**Class to manage the friends requests
 * @author Ruben Loaiza
 * @author Katherine Lindo
 * @author Mario Salazar
 */
public class FriendsRequestViewController {
    private static JComboBox listrequest;
    private static JLabel   jLNicknameuser;
/**Method to initiate the internal components of FriendRequestView
 * 
 * @param listRequest 
 * @param jLNickname 
 * 
 */
    public static void initOutlets(JComboBox listRequest,JLabel jLNickname){
        FriendsRequestViewController.listrequest=listRequest;
        FriendsRequestViewController.jLNicknameuser= jLNickname;
    }
/**Method to load all users invitation
 * 
 * @return boolean 
 */
    public static boolean loadAllUsersInCombo()
    {
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer;   
        
        objectToServer = new StandardObject("7:"+LoginViewController.jTFNickname.getText(), null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        final ListRequest la = (ListRequest) UtilFunctions.byteArrayToObject(answer);
         if (la==null){
            return false;
        }
        Iterator listIterator = la.iterator();
        
        listrequest.removeAllItems();
        
        while (listIterator.hasNext())
        {
            User actualUser = (User) listIterator.next();
            
            String information = actualUser.getFirstLastName()+ ", " + actualUser.getName()+", NickName"+ " : " + actualUser.getNickname();
            
            listrequest.addItem(information);  
            
        }
                      listrequest.addActionListener(new ActionListener() 
                      {
			@Override
			public void actionPerformed(ActionEvent e) {
                            String prueba= listrequest.getSelectedItem().toString();
                            String[] nickname=prueba.split(" : ");
                            System.out.println("Username: " + nickname[1]);
                           User usercombo=la.searchfriend(nickname[1]);
                            if (usercombo != null){
                                FriendsRequestViewController.jLNicknameuser.setText(usercombo.getNickname());

                             }
			}
                      });
     return true;}
    

 /**Method to asnswer the requests of friends
  * 
  * @param answeruser 
  */
    public static void answerRequest(String answeruser)
    {
        Preferences preferences=PreferencesXMLFile.readPreferencesFromUserDataBase();
        SocketClient clientRequest = new SocketClient();

        StandardObject objectToServer;
        byte [] answer;   
        objectToServer = new StandardObject("8:"+LoginViewController.jTFNickname.getText()+":"+FriendsRequestViewController.jLNicknameuser.getText()+":"+answeruser, null);
        answer = clientRequest.executeRequest(objectToServer,preferences.getUserip(), 7687);
        }
}
    


